//
//  ViewController.swift
//  PostaCourier
//
//  Created by Dan Rudolf on 7/13/18.
//  Copyright © 2018 com.rudolfmedia. All rights reserved.
//

import UIKit
import CoreLocation

class MainVC: UIViewController, UITableViewDataSource, UITableViewDelegate, SocketDataProtocol, AlertControllerDelegate {


    @IBOutlet weak var logoutButton: UIButton!
    @IBOutlet weak var menueTableView: UITableView!
    @IBOutlet weak var contentContainer: UIView!
    @IBOutlet weak var menuContainer: UIView!
    @IBOutlet weak var nameLabel: UILabel!

    let socketController = SocketController.sharedInstance
    let tripController = TripController.sharedInstnce
    let geoController = GeoController.sharedInstance
    let formatter = Formatter()
    let selectionTint = #colorLiteral(red: 0.09019607843, green: 0.7411764706, blue: 0.8705882353, alpha: 1)

    var menueVisible : Bool = false
    var visibleTab : UIViewController?
    var banner: BannerView!
    var tripRequestVC: TripRequestVC?


    var homeVC: HomeVC = {
        let vc = HomeVC.init(nibName: "HomeVC", bundle: Bundle.main)
        return vc
    }()

    var profileVC: UserProfileVC = {
        let vc = UserProfileVC.init(nibName: "UserProfileVC", bundle: Bundle.main)
        return vc
    }()

    var paymentVC: PaymentsVC = {
        let vc = PaymentsVC.init(nibName: "PaymentsVC", bundle: Bundle.main)
        return vc
    }()

    var deliveryHistoryVC: DeliveryHistoryVC = {
        let vc = DeliveryHistoryVC.init(nibName: "DeliveryHistoryVC", bundle: Bundle.main)
        return vc
    }()

    var termsVC: TermsVC = {
        let vc = TermsVC.init(nibName: "TermsVC", bundle: Bundle.main)
        return vc
    }()


    override func viewDidLoad() {
        super.viewDidLoad()
        menueTableView.dataSource = self
        menueTableView.delegate = self
        menueTableView.register(UINib.init(nibName: "MenueItemCell", bundle: Bundle.main), forCellReuseIdentifier: "MENUE_CELL")

        let dismissTap = UITapGestureRecognizer.init(target: self, action: #selector(dismissMenu))
        dismissTap.cancelsTouchesInView = false
        self.contentContainer.addGestureRecognizer(dismissTap)

        self.setViewControllerAsChildViewController(viewController: homeVC)
        homeVC.parentVC = self

        self.logoutButton.layer.cornerRadius = 8
        self.logoutButton.layer.borderColor = UIColor.black.cgColor
        self.logoutButton.layer.borderWidth = 1
        self.logoutButton.layer.masksToBounds = false
        self.socketController.dataDelegate = self
        self.nameLabel.text = UserController.currentUser.name.getNameString()

        menuContainer.setDropShadow(radius: 3, opacity: 0.25, offset: CGSize.init(width: 1, height: 0))

    }

    override func viewDidLayoutSubviews() {
        self.menuContainer.center = CGPoint.init(x: -(self.menuContainer.frame.width/2), y: self.view.center.y)
    }

    func addShadowAndCornerRadius(view: UIView){
        view.layer.shadowPath = UIBezierPath(roundedRect:
            view.bounds, cornerRadius: view.layer.cornerRadius).cgPath
        view.layer.shadowColor = UIColor.black.cgColor
        view.layer.shadowOpacity = 0.3
        view.layer.shadowOffset = CGSize(width: -2, height: 2)
        view.layer.shadowRadius = 10
        view.layer.masksToBounds = false
    }

    //Side Menue Delegation Fucntions
    private func setViewControllerAsChildViewController(viewController: UIViewController) {
        addChildViewController(viewController)
        contentContainer.addSubview(viewController.view)
        viewController.view.frame = contentContainer.bounds
        viewController.view.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
        viewController.didMove(toParentViewController: self)
        if let childTab = self.visibleTab {
            if childTab != viewController {
                removeViewControllerAsChildViewController(viewController: childTab)
            }
        }
        self.visibleTab = viewController
    }

    private func removeViewControllerAsChildViewController(viewController: UIViewController) {
        viewController.willMove(toParentViewController: nil)
        viewController.view.removeFromSuperview()
        viewController.removeFromParentViewController()
    }

    @objc func dismissMenu(){
        if self.menueVisible {
            shouledShowMenu(show: false)
        }
    }

    func shouledShowMenu(show: Bool){
        if show == true{
            self.menueVisible = true
            UIViewPropertyAnimator(duration: 0.2, curve: .easeOut) {
                self.menuContainer.center = CGPoint.init(x: (self.menuContainer.frame.width/2), y: self.view.center.y)
                self.contentContainer.alpha = 0.75
                self.contentContainer.transform = CGAffineTransform(scaleX: 0.92, y: 0.92)
                }.startAnimation()
        } else {
            self.menueVisible = false
            UIViewPropertyAnimator(duration: 0.2, curve: .easeIn) {
                self.menuContainer.center = CGPoint.init(x: -(self.menuContainer.frame.width/2), y: self.view.center.y)
                self.contentContainer.alpha = 1
                self.contentContainer.transform = CGAffineTransform(scaleX: 1, y: 1)
                }.startAnimation()
        }
    }

    @IBAction func onMenuPressed(_ sender: Any) {
        shouledShowMenu(show: true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    func onHomeSelected(){
        self.setViewControllerAsChildViewController(viewController: homeVC)
        self.shouledShowMenu(show: false)
    }

    func onDeliveryHistorySelected(){
        self.setViewControllerAsChildViewController(viewController: deliveryHistoryVC)
        self.shouledShowMenu(show: false)
    }

    func onProfileSelected(){
        self.setViewControllerAsChildViewController(viewController: profileVC)
        self.shouledShowMenu(show: false)
    }

    func onPaymentSelected(){
        self.setViewControllerAsChildViewController(viewController: paymentVC)
        self.shouledShowMenu(show: false)
    }
    func onTermsSelected(){
        self.setViewControllerAsChildViewController(viewController: termsVC)
        self.shouledShowMenu(show: false)
    }

    //MARK:  TableView datasouce/delegate
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    func tableView(_ tableView: UITableView, didHighlightRowAt indexPath: IndexPath) {
        if let cell = tableView.cellForRow(at: indexPath) as? MenueItemCell{
            cell.cellImageView.image = cell.cellImageView.image!.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
            cell.cellImageView.tintColor = selectionTint
            cell.cellTitleLabel.textColor = selectionTint
        }
    }
    func tableView(_ tableView: UITableView, didUnhighlightRowAt indexPath: IndexPath) {
        if let cell = tableView.cellForRow(at: indexPath) as? MenueItemCell{
            cell.cellImageView.image = cell.cellImageView.image!.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
            cell.cellImageView.tintColor = UIColor.black
            cell.cellTitleLabel.textColor = UIColor.black
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MENUE_CELL", for: indexPath) as! MenueItemCell
        switch indexPath.row {
        case 0:
            cell.topDivider.backgroundColor = UIColor.groupTableViewBackground
            cell.cellTitleLabel.text = "Home"
            cell.cellImageView.image = UIImage.init(named: "home_icon")
            break

        case 1:
            cell.cellTitleLabel.text = "Delivery History"
            cell.cellImageView.image = UIImage.init(named: "delivery_history_icon")
            break
        case 2:
            cell.cellTitleLabel.text = "Payment"
            cell.cellImageView.image = UIImage.init(named: "payment_icon_small")
            break
        case 3:
            cell.cellTitleLabel.text = "Settings"
            cell.cellImageView.image = UIImage.init(named: "settings_icon")
            break
        default:
            break
        }
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            self.onHomeSelected()
            break
        case 1:
            self.onDeliveryHistorySelected()
            break
        case 2:
            self.onPaymentSelected()
            break
        case 3:
            self.onProfileSelected()
            break
        default:
            break
        }
    }

    func didReceiveNewTripRequest(trip: Trip) {
        if UIApplication.shared.applicationState == .active {
            tripRequestVC = TripRequestVC.init(nibName: "TripRequestVC", bundle: Bundle.main)
            tripRequestVC!.modalPresentationStyle = .overCurrentContext
            tripRequestVC?.currentTrip = trip
//            tripRequestVC!.timeoutInterval = timeoutInterval
            self.present(tripRequestVC!, animated: false, completion: nil)
        } else{
            let delegate = UIApplication.shared.delegate as? AppDelegate
            let distance = self.formatter.getDistancbetween(current: self.geoController.lastLocation.coordinate, des: trip.pickupGeo.getCoordinates())
            delegate?.triggerNewTripNotification(trip: trip, distance: "\(distance) mi away")
        }
    }

//    func tripTimedOut() {
//        DispatchQueue.main.async {
//            self.tripRequestVC?.dismiss(animated: true, completion: nil)
//        }
//
//        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
//            self.tripRequestVC = nil
//        }
//    }

    func alertTripCancled(){
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
            let canceledVC = DeliveryCanceledVC.init(nibName: "DeliveryCanceledVC", bundle: Bundle.main)
            canceledVC.modalPresentationStyle = .overCurrentContext
            canceledVC.setCanceledAddress(street: self.tripController.currentTrip!.pickup.address.getStreet(type: .full))
            self.present(canceledVC, animated: true, completion: nil)
            self.tripController.getInprogressTrips()
        }

    }

    func didSelectCancel() { }

    func didSelectProceed(kDelegateTrigger: String) {
        if kDelegateTrigger == "ORDER_CANCLED"{
            self.tripController.getInprogressTrips()

        }
    }

    @IBAction func onLoggoutPressed(_ sender: Any) {
        UserController.sharedInstace.clean()
        AuthController.sharedInstance.clean()
        TripController.sharedInstnce.clean()
        AppDelegate.shared.applicationStateController.presentLogin()
    }
}


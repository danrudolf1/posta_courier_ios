////
////  CurrentTripVC.swift
////  PostaCourier
////
////  Created by Dan Rudolf on 7/25/18.
////  Copyright © 2018 com.rudolfmedia. All rights reserved.
////
//
//import UIKit
//import Mapbox
//
//class CurrentTripVC: UIViewController, MGLMapViewDelegate {
//
//    @IBOutlet weak var progressView: UIProgressView!
//    @IBOutlet weak var titleLabel: UILabel!
//    @IBOutlet weak var nameLabel: UILabel!
//    @IBOutlet weak var addressLineOneLabel: UILabel!
//    @IBOutlet weak var addressLineTwoLabel: UILabel!
//    @IBOutlet weak var insturctionsLabel: UILabel!
//    @IBOutlet weak var buttonTitleLabel: UILabel!
//    @IBOutlet weak var detailsContainer: UIView!
//    @IBOutlet weak var mapContainer: UIView!
//
//    let lightImpactFeedbackGenerator = UIImpactFeedbackGenerator(style: .heavy)
//    let userController = UserController.sharedInstace
//    let pickupMarker = MGLPointAnnotation()
//    let dropoffMarker = MGLPointAnnotation()
//    let loadingView = LoadingViewVC.init(nibName: "LoadingViewVC", bundle: Bundle.main)
//    let tripController = TripController.sharedInstnce
//
//    var loadingViewDelegate: LoadingViewProtocol?
//
//    var mapView : MGLMapView!
//    var currentTrip: Trip!
//    var timer = Timer.init()
//    var seconds = 2
//    var buttonText = ""
//    
//
//    override func viewDidLoad() {
//        super.viewDidLoad()
//
//        currentTrip = userController.currentTrip!
//        loadingView.modalPresentationStyle = .overCurrentContext
//
//        setupMapViewInitial()
//        addShadowAndCornerRadius(view: detailsContainer)
//        setPrgressViewInitial()
//        populateViewForState(trip: currentTrip)
//    }
//
//    override func viewDidAppear(_ animated: Bool) {
//        let window = view.window!
//        let gr0 = window.gestureRecognizers![0] as UIGestureRecognizer
//        let gr1 = window.gestureRecognizers![1] as UIGestureRecognizer
//        gr0.delaysTouchesBegan = false
//        gr1.delaysTouchesBegan = false
//    }
//
//    //MARK: View Rendering setup
//    func setPrgressViewInitial(){
//        let t = CGAffineTransform.init(scaleX: 1.1, y: 24.0)
//        self.progressView.transform = t
//    }
//
//    func resetProgressBar(){
//        self.seconds = 2
//        self.timer.invalidate()
//        self.progressView.setProgress(0, animated: false)
//        self.buttonTitleLabel.text = buttonText
//    }
//
//    func startProgressAnimation(){
//        self.buttonTitleLabel.text = "Keep Pressing"
//        timer = Timer.scheduledTimer(timeInterval: 0.85, target: self, selector: #selector(updateTimer), userInfo: nil, repeats: true)
//
//        UIView.animate(withDuration: 3, animations: {
//            self.progressView.setProgress(100, animated: true)
//        })
//    }
//
//    @objc func updateTimer() {
//        if seconds < 1 {
//            self.seconds = 2
//            self.timer.invalidate()
//            self.lightImpactFeedbackGenerator.prepare()
//            self.lightImpactFeedbackGenerator.impactOccurred()
//            self.buttonTitleLabel.text = "DONE!"
//            self.updateTrip(trip: self.currentTrip)
//        } else {
//            seconds -= 1
//        }
//    }
//
//    func addShadowAndCornerRadius(view: UIView){
//        var shadowLayer: CAShapeLayer!
//        shadowLayer = CAShapeLayer()
//        shadowLayer.path = UIBezierPath(roundedRect: view.bounds, cornerRadius: 0).cgPath
//        shadowLayer.fillColor = UIColor.white.cgColor
//        shadowLayer.shadowColor = UIColor.darkGray.cgColor
//        shadowLayer.shadowPath = shadowLayer.path
//        shadowLayer.shadowOffset = CGSize(width: -4.0, height: 4.0)
//        shadowLayer.shadowOpacity = 0.45
//        shadowLayer.shadowRadius = 10
//
//        view.layer.insertSublayer(shadowLayer, at: 0)
//    }
//
//    //MARK: MapView Delegate
//    func setupMapViewInitial(){
//        mapView = MGLMapView(frame: mapContainer.bounds)
//        mapView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
//        mapView.styleURL = URL.init(string: "mapbox://styles/danrudolf/cix131v4c00112qpb7zqy1f2e")
//        mapView.delegate = self
//        mapView.allowsTilting = false
//        mapView.allowsRotating = false
//        mapView.zoomLevel = 16
//        mapView.tintColor = #colorLiteral(red: 0.4392156863, green: 0.3529411765, blue: 0.937254902, alpha: 1)
//        mapView.isUserInteractionEnabled = true
//        mapView.showsUserLocation = true
//        mapContainer.addSubview(mapView)
//    }
//
//    func setMapViewForPickup(trip: Trip){
//        self.mapView.removeAnnotation(dropoffMarker)
//        pickupMarker.coordinate = trip.pickupGeo.getCoordinates()
//        pickupMarker.title = "Pickup"
//        pickupMarker.subtitle = trip.pickup.address.street
//        mapView.addAnnotation(pickupMarker)
//        mapView.selectAnnotation(pickupMarker, animated: true)
//        self.mapView.setCenter(pickupMarker.coordinate, animated: true)
//
//    }
//
//    func setMapViewForDropoff(trip: Trip){
//        self.mapView.removeAnnotation(pickupMarker)
//        dropoffMarker.coordinate = trip.destintationGeo.getCoordinates()
//        dropoffMarker.title = "Dropoff"
//        dropoffMarker.subtitle = trip.destination.address.street
//        mapView.addAnnotation(dropoffMarker)
//        mapView.selectAnnotation(dropoffMarker, animated: true)
//        self.mapView.setCenter(dropoffMarker.coordinate, animated: true)
//    }
//
//    func mapView(_ mapView: MGLMapView, annotationCanShowCallout annotation: MGLAnnotation) -> Bool {
//        return true
//    }
//
//    func mapView(_ mapView: MGLMapView, viewFor annotation: MGLAnnotation) -> MGLAnnotationView? {
//        if  annotation is MGLUserLocation{
//            return nil
//        }
//        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: "MARKER")
//        if (annotationView == nil){
//            annotationView = MGLAnnotationView.init(frame: CGRect.init(x: 0, y: 0, width: 15, height: 15))
//            annotationView?.isEnabled = true
//            let imageView = UIImageView.init(frame: (annotationView?.frame)!)
//            imageView.contentMode = .scaleAspectFit
//            if (annotation.coordinate.latitude == currentTrip.pickupGeo.getCoordinates().latitude){
//                imageView.image = UIImage.init(named: "pickup_marker")
//            }
//            if (annotation.coordinate.latitude == currentTrip.destintationGeo.getCoordinates().latitude){
//                imageView.image = UIImage.init(named: "des_marker")
//            }
//            annotationView?.addSubview(imageView)
//        }
//        return annotationView
//    }
//
//    //MARK: Data Rendering
//    func populateViewForState(trip: Trip){
//        DispatchQueue.main.async {
//            switch trip.status {
//            case 2:
//                self.populateInRouteLabels(trip: trip)
//                self.setMapViewForPickup(trip: trip)
//                break
//            case 3:
//                self.populateReceivedLabels(trip: trip)
//                self.setMapViewForDropoff(trip: trip)
//                break
//            default:
//                break
//            }
//        }
//    }

//    func populateInRouteLabels(trip: Trip){
//        titleLabel.text = "Pickup Location"
//        nameLabel.text = trip.pickup.name.getNameString()
//        addressLineOneLabel.text = trip.pickup.address.street
//        addressLineTwoLabel.text = trip.pickup.address.getCityZipString()
//        buttonTitleLabel.text = "Received"
//        buttonText = buttonTitleLabel.text!
//        insturctionsLabel.text = trip.getInstructionsString()
//    }
//
//    func populateReceivedLabels(trip: Trip){
//        titleLabel.text = "Dropoff Location"
//        nameLabel.text = trip.destination.name.getNameString()
//        addressLineOneLabel.text = trip.destination.address.street
//        addressLineTwoLabel.text = trip.destination.address.getCityZipString()
//        buttonTitleLabel.text = "Delivered"
//        buttonText = buttonTitleLabel.text!
//        insturctionsLabel.text = trip.getInstructionsString()
//    }
//
//    func populateDeliveredLabels(trip: Trip){
//
//    }
//
//    //MARK: Actions and Selectors
//    @IBAction func onDismissPressed(_ sender: Any) {
//        self.dismiss(animated: true, completion: nil)
//    }
//    @IBAction func didPressOptions(_ sender: Any) {
//        DispatchQueue.main.async {
//            let title = "Options - \(self.currentTrip.tripId)"
//            let optionsController = UIAlertController.init(title: title, message: nil, preferredStyle: .actionSheet)
//            let call = UIAlertAction.init(title: "Call Sender", style: .default) { (action) in
//
//            }
//            let contact = UIAlertAction.init(title: "Cant't Complete Delivery", style: .default) { (action) in
//
//            }
//            let done = UIAlertAction.init(title: "Cancel", style: .cancel) { (action) in
//
//            }
//            optionsController.addAction(call)
//            optionsController.addAction(contact)
//            optionsController.addAction(done)
//            self.present(optionsController, animated: true, completion: nil)
//        }
//    }
//
//    func dismissLoadingView(){
//        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1.0 ) {
//            self.loadingView.dismiss(animated: false, completion: nil)
//        }
//    }
//
//    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
//        if let touch = touches.first{
//            let position = touch.location(in: self.progressView)
//            if position.y > 0{
//                startProgressAnimation()
//            }
//        }
//    }
//
//    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
//        resetProgressBar()
//    }
//
//    func updateTrip(trip: Trip){
//        switch trip.status {
//        case 2:
//            self.setReceived(trip: trip)
//            self.populateViewForState(trip: trip)
//            break
//        case 3:
////            self.setDelivered(trip: trip)
//            let des = ConfirmCodeVC.init(nibName: "ConfirmCodeVC", bundle: Bundle.main)
//            des.currentTrip = self.currentTrip
//            present(des, animated: true, completion: nil)
//            break
//        default:
//            break
//        }
//    }
//
//    func setReceived(trip: Trip){
//        self.present(loadingView, animated: false, completion: nil)
//        tripController.updateTripStatus(trip_id: currentTrip.id, status: 3, confirmCode: nil) { (trip, err) in
//            self.dismissLoadingView()
//            if (err != nil) {
//                //alert err
//                print(err)
//            }
//            guard let updated = trip else{
//                return
//            }
//            self.currentTrip = updated
//            self.populateViewForState(trip: updated)
//        }
//    }
//
//    func setDelivered(trip: Trip){
//        self.present(loadingView, animated: false, completion: nil)
//        tripController.updateTripStatus(trip_id: currentTrip.id, status: 4, confirmCode: "1234") { (trip, err) in
//            self.dismissLoadingView()
//            if (err != nil) {
//                //alert err
//            }
//            guard let updated = trip else{
//                return
//            }
//            self.currentTrip = updated
//            self.populateViewForState(trip: updated)
//        }
//    }
//}

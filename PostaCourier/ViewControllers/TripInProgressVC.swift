//
//  TripInProgressVC.swift
//  PostaCourier
//
//  Created by Dan Rudolf on 11/1/18.
//  Copyright © 2018 com.rudolfmedia. All rights reserved.
//

import UIKit
import Mapbox

class TripInProgressVC: UIViewController, SnappyCardProtocol {

    @IBOutlet weak var mapContainer: UIView!
    @IBOutlet weak var closeButton: UIButton!

    let formatter = Formatter()
    let postaTheme = PostaTheme()

    var currentTrip: Trip = TripController.sharedInstnce.currentTrip!
    var mapController: MapController!
    var cardContents: InProgressTripView!
    var card: SnappyCard!

    override func viewDidLoad() {
        super.viewDidLoad()

        self.mapController = MapController.init(view: mapContainer,
                                                parent: self,
                                                tracking: .following,
                                                trip: currentTrip,
                                                from: .tripInProgress)

    }

    override func viewDidLayoutSubviews() {

        postaTheme.applyButtonShadow(view: closeButton)
        self.closeButton.layer.cornerRadius = 6

        if cardContents == nil {
            cardContents = InProgressTripView.init(frame: CGRect.init(origin: CGPoint.zero, size: CGSize.init(width: self.view.frame.width, height: 305)), trip: currentTrip, parent: self)
            cardContents.populateDetailsForTripState(trip: currentTrip)
        }
        if card == nil {
            card = SnappyCard.init(parent: self, content: cardContents, verticalBound: 145)
            card.delegate = self
            self.view.addSubview(card)
            card.setCollapsed()
        }
    }

    func didCollapseCard() { }

    func didExpantCard() { }

    //MARK: Actions and Selectors
    @IBAction func onBackPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }

    //MARK: View Rendering and apperance
    func addShadowAndCornerRadius(view: UIView){
        var shadowLayer: CAShapeLayer!
        shadowLayer = CAShapeLayer()
        shadowLayer.path = UIBezierPath(roundedRect: view.bounds, cornerRadius: 0).cgPath
        shadowLayer.fillColor = UIColor.white.cgColor

        shadowLayer.shadowColor = UIColor.darkGray.cgColor
        shadowLayer.shadowPath = shadowLayer.path
        shadowLayer.shadowOffset = CGSize(width: -4.0, height: 4.0)
        shadowLayer.shadowOpacity = 0.65
        shadowLayer.shadowRadius = 10

        view.layer.insertSublayer(shadowLayer, at: 0)
    }

}


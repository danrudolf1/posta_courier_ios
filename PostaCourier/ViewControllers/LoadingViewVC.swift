//
//  LodingViewVC.swift
//  PostaCourier
//
//  Created by Dan Rudolf on 7/26/18.
//  Copyright © 2018 com.rudolfmedia. All rights reserved.
//

import UIKit

protocol LoadingViewProtocol {
    func dismiss()
}

class LoadingViewVC: UIViewController {

    @IBOutlet weak var loadingView: UIView!
    @IBOutlet weak var alphaMaskView: UIView!

    var presenting: UIViewController!
    var delegate: LoadingViewProtocol?

    override func viewDidLoad() {
        super.viewDidLoad()
        loadingView.backgroundColor = UIColor.clear
        self.modalPresentationStyle = .overCurrentContext
    }

    func dismissDelayed() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.8) {
            self.dismiss(animated: false, completion: nil)
        }
    }
    func dismissImediate() {
        DispatchQueue.main.async {
            self.dismiss(animated: false, completion: nil)
        }
    }
    func show(){
        DispatchQueue.main.async {
            self.presenting.present(self, animated: false, completion: nil)
        }
    }

    override func viewDidAppear(_ animated: Bool) {
        loadingView.backgroundColor = UIColor.black
        loadingView.alpha = 0.8
        loadingView.round(corners: .allCorners, radius: 10)
        alphaMaskView.backgroundColor = UIColor.black
        alphaMaskView.alpha = 0.15
    }

}


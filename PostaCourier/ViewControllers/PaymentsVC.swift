//
//  PaymentsVC.swift
//  PostaCourier
//
//  Created by Dan Rudolf on 7/13/18.
//  Copyright © 2018 com.rudolfmedia. All rights reserved.
//

import UIKit
import SafariServices
import WebKit

let kStripeLive = "ca_E0a3VMfN6AZNHNtjfbVjiSm0RsyfnJl9"
let kStripeTest = "ca_E0a3kdmZmz3vYxPkPvc35F5FMuuyIZCk"


class PaymentsVC: UIViewController, WKNavigationDelegate {

    @IBOutlet weak var titleBarView: UIView!
    @IBOutlet weak var webView: WKWebView!

    let kSafariViewControllerCloseNotification = "kSafariViewControllerCloseNotification"
    let kBaseConnectURL = "https://connect.stripe.com/express/oauth/authorize?client_id=\(kStripeLive)&state="
    let loadingView = LoadingViewVC.init(nibName: "LoadingViewVC", bundle: Bundle.main)
    let paymentController = PaymentController()
    let userController = UserController.sharedInstace
    let postaTheme = PostaTheme()

    var safariVC: SFSafariViewController?
    var hasAccount = false

    override func viewDidLoad() {
        super.viewDidLoad()
        loadingView.modalPresentationStyle = .overCurrentContext
        webView.navigationDelegate = self
    }

    override func viewDidLayoutSubviews() {
        postaTheme.applyDropShadowForTitleBar(view: titleBarView)
    }

    //MARK: Actions and Selectors
    @IBAction func onMenuPress(_ sender: Any) {
        guard let main = self.parent as? MainVC else{
            return
        }
        main.shouledShowMenu(show: true)
    }

    override func viewWillAppear(_ animated: Bool) {
        loadSafariView()
    }

    //MARK: WebKit Delegate
    func loadSafariView(){
        let banner = BannerView.init(parent: self)
        banner.setDurationAndLevel(duration: .short, level: .warn)

        guard UserController.currentUser.stripeAccount != nil else {
            let url = "\(kBaseConnectURL)\(UserController.currentUser.id)"
            let req = URLRequest.init(url: URL.init(string: url)!)
            webView.load(req)
            return
        }

        paymentController.getStripeDashLink { (link, err) in
            guard err == nil else{
                banner.show(message: "Error retrieving account details, please try again.")
                return
            }
            let req = URLRequest.init(url: link!)
            DispatchQueue.main.async {
                self.webView.load(req)
            }
        }
    }

    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        loadingView.dismiss(animated: false, completion: nil)
        let strVal = webView.url?.absoluteString
        if (strVal!.contains("stripe/connect_courier")){
            self.userController.refreshUserData { (succcess) in
                if (succcess == true){
                    self.loadStripeDash()
                }
            }
        }
    }

    func loadStripeDash(){
        paymentController.getStripeDashLink { (link, err) in
            guard err == nil else{
                DispatchQueue.main.async {
                    let banner = BannerView.init(parent: self)
                    banner.setDurationAndLevel(duration: .short, level: .warn)
                    banner.show(message: "Error retrieving account details, please try again.")
                }
                return
            }
            let req = URLRequest.init(url: link!)
            DispatchQueue.main.async {
                self.webView.load(req)
            }
        }
    }

    func safariViewControllerDidFinish(_ controller: SFSafariViewController) {
        controller.dismiss(animated: true, completion: nil)
    }

    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        self.present(loadingView, animated: false, completion: nil)
    }

}


//
//  TripRequestVC.swift
//  PostaCourier
//
//  Created by Dan Rudolf on 11/1/18.
//  Copyright © 2018 com.rudolfmedia. All rights reserved.
//

import UIKit
import Mapbox

class TripRequestVC: UIViewController, MGLMapViewDelegate {

    @IBOutlet weak var verticalSeporator: UIView!
    @IBOutlet weak var timerContainer: UIView!
    @IBOutlet weak var distanceAwayLabel: UILabel!
    @IBOutlet weak var dropoffAddressLabel: UILabel!
    @IBOutlet weak var pickupAddressLabel: UILabel!
    @IBOutlet weak var dropoffDot: UIView!
    @IBOutlet weak var pickupDot: UIView!
    @IBOutlet weak var acceptTripButton: UIButton!
    @IBOutlet weak var declineTripButton: UIButton!

    let postaTheme = PostaTheme()

    var timerView: RoundTimerView?
    var currentTrip: Trip?
    var socketController = SocketController.sharedInstance
    var formatter = Formatter()
    var geoController = GeoController.sharedInstance
    var content: UIView!
    var mapView : MGLMapView!


    override func viewDidLoad() {
        super.viewDidLoad()
        setLabels()
    }

    override func viewDidLayoutSubviews() {
        if timerView == nil {
            timerView = RoundTimerView.init(frame: timerContainer.bounds)
            timerContainer.addSubview(timerView!)
        }

        if content == nil {
            content = UIView.init(frame: CGRect.init(x: 0, y: 0, width: timerContainer.frame.width - 30, height: timerContainer.frame.height - 30))
            content.center = timerContainer.center
            content.round(corners: .allCorners, radius: content.frame.height/2)
            self.setUpMapview(container: content)
            self.view.addSubview(content)
        }

        postaTheme.fillVerticalGradient(view: verticalSeporator)
        acceptTripButton.round(corners: .allCorners, radius: 5)
        declineTripButton.round(corners: .allCorners, radius: 5)

        pickupDot.setCircleView()
        dropoffDot.setCircleView()
    }

    func isModal() -> Bool {
        if self.presentingViewController != nil {
            return true
        } else if self.navigationController?.presentingViewController?.presentedViewController == self.navigationController  {
            return true
        } else if self.tabBarController?.presentingViewController is UITabBarController {
            return true
        }

        return false
    }

    override func viewDidAppear(_ animated: Bool) {
        timerView?.animateWithDuration(duration: Double.init(exactly: socketController.seconds + 2)!)
        NotificationCenter.default.addObserver(self, selector: #selector(TripRequestVC.onDeclinePressed(_:)), name: .tripDidTimeout, object: nil)
    }

    override func viewDidDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
    }

    //Data Population
    func setUpMapview(container: UIView){
        let scaledcontainer = CGRect.init(x: -10, y: -10, width: container.frame.width + 20, height: container.frame.width + 20)

        let mapController = MapController.init(view: UIView.init(frame: scaledcontainer),
                                               parent: self,
                                               trip: currentTrip!,
                                               from: .tripRequest)
        self.mapView = mapController.mapView
        container.addSubview(mapView)
    }

    func setLabels(){
        pickupAddressLabel.text = currentTrip!.pickup.address.getStreet(type: .full)
        dropoffAddressLabel.text = currentTrip!.destination.address.getStreet(type: .full)
        let current = geoController.lastLocation.coordinate
        let des = currentTrip!.pickupGeo.getCoordinates()
        let distance = formatter.getDistancbetween(current: current, des: des)
        distanceAwayLabel.text = "\(distance)mi away from your location"
    }


    //View Rendering
    @IBAction func onDeclinePressed(_ sender: Any) {
        print("Timeout Trigger")
        socketController.declineTrip()
        self.dismiss(animated: true, completion: nil)

    }
    
    @IBAction func onAcceptPresed(_ sender: Any) {
        socketController.acceptTrip()
        self.dismiss(animated: true, completion: nil)
    }

}

//
//  LoginVC.swift
//  Posta
//
//  Created by Dan Rudolf on 6/15/18.
//  Copyright © 2018 com.rudolfmedia. All rights reserved.
//

import UIKit
import CoreData
import Lottie


class LoginVC: UIViewController {

    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var lottieContainer: UIView!
    @IBOutlet weak var forgotPasswordButton: UIButton!


    let userController = UserController.sharedInstace
    let authController = AuthController.sharedInstance

    override func viewDidLoad() {
        super.viewDidLoad()
        roundViews(view: loginButton)
        roundViews(view: passwordTextField)
        roundViews(view: usernameTextField)
        activityIndicator.isHidden = true

        let responderTap = UITapGestureRecognizer.init(target: self, action: #selector(resignResponders))
        self.view.addGestureRecognizer(responderTap)
        checkReturning()
    }

    @objc func resignResponders(){
        self.usernameTextField.resignFirstResponder()
        self.passwordTextField.resignFirstResponder()
    }

    func roundViews(view: UIView){
        view.layer.masksToBounds = false
        view.layer.cornerRadius = 4
    }

    func hideViews(){
        self.usernameTextField.isHidden = true
        self.passwordTextField.isHidden = true
        self.loginButton.isHidden = true
    }

    func showViews(){
        self.usernameTextField.isHidden = false
        self.passwordTextField.isHidden = false
        self.loginButton.isHidden = false
    }

    func checkReturning(){
        guard let username = authController.getCurrentUserName() else{
            showViews()
            return
        }
        self.usernameTextField.text = username

        guard let password = authController.getCurrentPassword() else{
            showViews()
            return
        }
        self.passwordTextField.text = password
        if password.isEmpty || username.isEmpty{
            showViews()
            return
        }
        loginUser(username: username, password: password)
    }

    func showLoginButton(){
        DispatchQueue.main.async {
            self.showViews()
            self.forgotPasswordButton.isHidden = false
            self.activityIndicator.isHidden = true
        }
    }

    func hideLoginButton(){
        DispatchQueue.main.async {
            self.hideViews()
            self.forgotPasswordButton.isHidden = true
            self.activityIndicator.isHidden = false
        }
    }

    func normalizeString(str: String) -> String{
        return str.lowercased().trimmingCharacters(in: .whitespacesAndNewlines)
    }

    @IBAction func onLoginPressed(_ sender: Any) {
        self.loginUser(username: normalizeString(str: usernameTextField.text ?? ""), password: passwordTextField.text ?? "")
    }

    func alertErr(err: Err){
        DispatchQueue.main.async {
            let alert = UIAlertController(title: "Couldn't Login", message: err.message, preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Okay", style: UIAlertActionStyle.default,handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }

    @IBAction func onForgotPasswordPressed(_ sender: Any) {
        let des = ResetPasswordVC.init(nibName: "ResetPasswordVC", bundle: Bundle.main)
        present(des, animated: true, completion: nil)
    }

    func presentApplicationRoot(){
        DispatchQueue.main.async {
            AppDelegate.shared.applicationStateController.presentApplicationLoader()
        }
    }

    func loginUser(username: String, password: String){
        hideLoginButton()
        userController.loginUser(user: username, password: password) { (user, err) in
            if user != nil{
                UserController.currentUser = user!
                self.authController.setNewUsername(username: username)
                self.authController.setNewPassword(password: password)
                self.presentApplicationRoot()

            } else if err != nil{
                self.showLoginButton()
                self.alertErr(err: err!)
            }
        }
    }

}

//
//  ConfirmCodeVC.swift
//  PostaCourier
//
//  Created by Dan Rudolf on 7/26/18.
//  Copyright © 2018 com.rudolfmedia. All rights reserved.
//

import UIKit

class ConfirmCodeVC: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var addressLineOneLabel: UILabel!
    @IBOutlet weak var addressLineTwoLabel: UILabel!
    @IBOutlet weak var confirmTextField: UITextField!
    @IBOutlet weak var instructionsLabel: UILabel!
    
    let loadingView = LoadingViewVC.init(nibName: "LoadingViewVC", bundle: Bundle.main)
    let userController = UserController.sharedInstace
    let tripController = TripController.sharedInstnce
    var currentTrip: Trip!
    var confirmCode: String!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.confirmTextField.becomeFirstResponder()
        self.confirmTextField.round(corners: .allCorners, radius: 4)
        self.confirmCode = "\(currentTrip.confirmCode!)"
        loadingView.modalPresentationStyle = .overCurrentContext
        populateInfo()
    }

    //MARK: Actions and Selectors
    @IBAction func codeValueDidChange(_ sender: Any) {
        if self.confirmTextField.text?.count == confirmCode.count{
            checkConfirmCodeValue(enteredVal: self.confirmTextField.text!)
        }
    }

    @IBAction func onCancelPressed(_ sender: Any) {
        self.confirmTextField.resignFirstResponder()
        self.dismiss(animated: true, completion: nil)
    }

    func populateInfo(){
        nameLabel.text = currentTrip.destination.name.getNameString()
        addressLineOneLabel.text = currentTrip.destination.address.getStreet(type: .full)
        addressLineTwoLabel.text = currentTrip.destination.address.getLocaleAddress(type: .short)
    }

    func checkConfirmCodeValue(enteredVal: String){
        self.present(loadingView, animated: false, completion: nil)
        if enteredVal == self.confirmCode{
            self.setTripDelivered()
        } else{
            hideLoadingViewWithFail()
        }
    }

    func setTripDelivered(){
        let banner = BannerView.init(parent: self)
        banner.setDurationAndLevel(duration: .short, level: .warn)
        tripController.updateTripStatus(trip_id: currentTrip.id, status: 4, confirmCode: self.confirmCode) { (trip, err) in
            self.loadingView.dismissDelayed()
            guard err == nil else {
                banner.showDelayed(message: "Something went wrong checking the confirm code, please try it again.")
                return
            }
            self.dismissNavigationStack()
        }

    }

    //MARK: View Rendering
    func showConfrimCodeFail(){
        let banner = BannerView.init(parent: self)
        banner.setDurationAndLevel(duration: .short, level: .warn)
        banner.showDelayed(message: "That confirmation code doesnt match the one on record.")
        self.confirmTextField.text = ""
    }

    func hideLoadingViewWithFail(){
        self.loadingView.dismissDelayed()
        self.showConfrimCodeFail()
    }

    func dismissNavigationStack(){
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1) {
            self.confirmTextField.resignFirstResponder()
            self.tripController.getInprogressTrips()
            self.confirmTextField.resignFirstResponder()
            self.loadingView.dismiss(animated: false, completion: nil)
            let vc = self.presentingViewController?.presentingViewController as! ApplicationStateController
            vc.dismiss(animated: true, completion: nil)
        }
    }
}

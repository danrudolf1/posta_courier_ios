//
//  UpdateAddressVC.swift
//  PostaCourier
//
//  Created by Dan Rudolf on 10/19/18.
//  Copyright © 2018 com.rudolfmedia. All rights reserved.
//

import UIKit

class UpdateAddressVC: UIViewController {

    @IBOutlet weak var titleBarView: UIView!
    @IBOutlet weak var streetTextField: UITextField!
    @IBOutlet weak var cityTextField: UITextField!
    @IBOutlet weak var stateTextField: UITextField!
    @IBOutlet weak var zipTextField: UITextField!

    let userController = UserController.sharedInstace
    let postaTheme = PostaTheme()

    override func viewDidLoad() {
        super.viewDidLoad()
        setTextfields(user: UserController.currentUser)
    }

    override func viewDidLayoutSubviews() {
        postaTheme.applyDropShadowForTitleBar(view: titleBarView)
    }

    func setTextfields(user: User){
        streetTextField.text = user.address.street
        streetTextField.becomeFirstResponder()
        cityTextField.text = user.address.city
        stateTextField.text = user.address.state
        zipTextField.text = user.address.zip
    }

    @IBAction func onCancelPressed(_ sender: Any) {
        self.resignResponders()
        self.dismiss(animated: true, completion: nil)
    }

    func checkInputValue(textfield: UITextField, trim: CharacterSet) -> String?{
        guard let val = textfield.text else{
            return nil
        }
        guard !val.isEmpty else{
            return nil
        }
        return val.trimmingCharacters(in: trim)
    }

    @IBAction func onSavePressed(_ sender: Any) {
        self.resignResponders()

        let banner = BannerView.init(parent: self)
        banner.setDurationAndLevel(duration: .short, level: .warn)

        let loadingView = LoadingViewVC.init(nibName: "LoadingViewVC", bundle: Bundle.main)
        loadingView.modalPresentationStyle = .overCurrentContext

        guard let street = checkInputValue(textfield: streetTextField, trim: .newlines) else {
            banner.show(message: "Please enter your street address.")
            return
        }
        guard let city = checkInputValue(textfield: cityTextField, trim: .newlines) else {
            banner.show(message: "Please enter your city.")
            return
        }
        guard let state = checkInputValue(textfield: stateTextField, trim: .whitespacesAndNewlines) else {
            banner.show(message: "Please enter your state.")
            return
        }
        guard let zip = checkInputValue(textfield: zipTextField, trim: .whitespacesAndNewlines) else {
            banner.show(message: "Please enter your zip code.")
            return
        }

        let address = Address.init(street: street, city: city, state: state, zip: zip)

        present(loadingView, animated: false, completion: nil)
        userController.updateUsersAddress(address: address) { (success, err) in
            loadingView.dismissDelayed()
            guard err == nil else{
                banner.showDelayed(message: err!.message)
                return
            }
            self.dismissView()
        }
    }

    func dismissView(){
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
            self.dismiss(animated: true, completion: nil)
        })
    }

    func resignResponders(){
        let responders = [streetTextField!, cityTextField!, stateTextField!, zipTextField!]
        for r in responders{
            if r.canResignFirstResponder{
                r.resignFirstResponder()
            }
        }
    }

}

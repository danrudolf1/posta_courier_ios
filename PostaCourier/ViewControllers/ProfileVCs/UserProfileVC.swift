//
//  UserProfileVC.swift
//  Posta
//
//  Created by Dan Rudolf on 10/13/18.
//  Copyright © 2018 com.rudolfmedia. All rights reserved.
//

import UIKit

class UserProfileVC: UIViewController, ProfileSyncDelegate {

    @IBOutlet weak var circleView: UIView!
    @IBOutlet weak var initialsLabel: UILabel!
    @IBOutlet weak var titleNameLabel: UILabel!
    @IBOutlet weak var userDateLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var streetLabel: UILabel!

    let userController = UserController.sharedInstace
    let formatter = Formatter()

    deinit {
        userController.removeDelegate(delegate: self)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setDropShadow(view: circleView)
        self.circleView(view: circleView)
        userController.addDelegate(delegate: self)
        populateDetails()
    }

    func didUpdateProfileData() {
        populateDetails()
    }

    func populateDetails(){
        let user = UserController.currentUser!
        let initials = "\(user.name.first.prefix(1).capitalized)\(user.name.last.prefix(1).capitalized)"
        initialsLabel.text = initials
        titleNameLabel.text = user.name.getNameString()
        nameLabel.text = user.name.getNameString()
        phoneLabel.text = user.phone!
        emailLabel.text = user.email!
        streetLabel.text = user.address.getStreet(type: .full)
        let joined = formatter.getJoinedDate(str: user.joinedDate)
        userDateLabel.text = "Joined Posta \(joined)"
    }

    //MARK: Actions and Selectors
    @IBAction func onMenuPress(_ sender: Any) {
        guard let main = self.parent as? MainVC else{
            return
        }
        main.shouledShowMenu(show: true)
    }
    @IBAction func onEditNamePressed(_ sender: Any) {
        let des = UpdateNameVC.init(nibName: "UpdateNameVC", bundle: Bundle.main)
        self.present(des, animated: true, completion: nil)
    }
    @IBAction func onEditEmailPressed(_ sender: Any) {
        let des = UpdateEmailVC.init(nibName: "UpdateEmailVC", bundle: Bundle.main)
        self.present(des, animated: true, completion: nil)
    }
    @IBAction func onEditPhonePressed(_ sender: Any) {
        let des = UpdatePhoneVC.init(nibName: "UpdatePhoneVC", bundle: Bundle.main)
        self.present(des, animated: true, completion: nil)
    }
    @IBAction func onEditAddressPressed(_ sender: Any) {
        let des = UpdateAddressVC.init(nibName: "UpdateAddressVC", bundle: Bundle.main)
        self.present(des, animated: true, completion: nil)
    }
    @IBAction func onChangePasswordPressed(_ sender: Any) {
        let des = ChangePasswordVC.init(nibName: "ChangePasswordVC", bundle: Bundle.main)
        self.present(des, animated: true, completion: nil)
    }
    @IBAction func onTermsPressed(_ sender: Any) {
        let des = TermsVC.init(nibName: "TermsVC", bundle: Bundle.main)
        self.present(des, animated: true, completion: nil)
    }


    //MARK: View Rendering
    func addShadow(view: UIView){
        var shadowLayer: CAShapeLayer!
        shadowLayer = CAShapeLayer()
        shadowLayer.path = UIBezierPath(roundedRect: view.bounds, cornerRadius: 2).cgPath
        shadowLayer.fillColor = UIColor.white.cgColor

        shadowLayer.shadowColor = UIColor.darkGray.cgColor
        shadowLayer.shadowPath = shadowLayer.path
        shadowLayer.shadowOffset = CGSize(width: 0.0, height: 2.0)
        shadowLayer.shadowOpacity = 0.4
        shadowLayer.shadowRadius = 2

        view.layer.insertSublayer(shadowLayer, at: 0)
    }

    func circleView(view: UIView){
        view.layer.masksToBounds = false
        view.layer.cornerRadius = (view.frame.height/2)
    }

    func setDropShadow(view: UIView){
        view.layer.shadowColor = UIColor.black.cgColor
        view.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        view.layer.shadowOpacity = 0.45
        view.layer.shadowRadius = 4
    }
}

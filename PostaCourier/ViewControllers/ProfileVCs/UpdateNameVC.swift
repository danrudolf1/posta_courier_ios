//
//  UpdateNameVC.swift
//  Posta
//
//  Created by Dan Rudolf on 10/17/18.
//  Copyright © 2018 com.rudolfmedia. All rights reserved.
//

import UIKit

class UpdateNameVC: UIViewController {

    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var titleBarView: UIView!
    @IBOutlet weak var lastNameTextField: UITextField!

    let userController = UserController.sharedInstace
    let postaTheme = PostaTheme()

    override func viewDidLoad() {
        super.viewDidLoad()
        setTextfields(user: UserController.currentUser)
    }

    override func viewDidLayoutSubviews() {
        postaTheme.applyDropShadowForTitleBar(view: titleBarView)
    }

    func setTextfields(user: User){
        firstNameTextField.text = user.name.first
        firstNameTextField.becomeFirstResponder()
        lastNameTextField.text = user.name.last
    }

    @IBAction func onCancelPressed(_ sender: Any) {
        self.resignResponders()
        self.dismiss(animated: true, completion: nil)
    }

    @IBAction func onSavePressed(_ sender: Any) {
        resignResponders()
        let banner = BannerView.init(parent: self)
        banner.setDurationAndLevel(duration: .short, level: .warn)

        let loadingView = LoadingViewVC.init(nibName: "LoadingViewVC", bundle: Bundle.main)
        loadingView.modalPresentationStyle = .overCurrentContext

        guard var first = firstNameTextField.text else{
            banner.show(message: "Please enter your first name")
            return
        }
        guard !first.isEmpty else{
            banner.show(message: "Please enter your first name")
            return
        }
        first = first.trimmingCharacters(in: .whitespacesAndNewlines)

        guard var last = lastNameTextField.text else{
            banner.show(message: "Please enter your last name")
            return
        }
        last = last.trimmingCharacters(in: .whitespacesAndNewlines)
        guard !last.isEmpty else{
            banner.show(message: "Please enter your last name")
            return
        }
        let name = Name.init(first: first, last: last)

        present(loadingView, animated: false, completion: nil)
        userController.updateUsersName(name: name) { (success, err) in
            loadingView.dismissDelayed()
            guard err == nil else{
                banner.showDelayed(message: err!.message)
                return
            }
            self.dismissView()
        }
    }

    func dismissView(){
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
            self.dismiss(animated: true, completion: nil)
        })
    }
    

    func resignResponders(){
        guard !firstNameTextField.isFirstResponder else{
            firstNameTextField.resignFirstResponder()
            return
        }
        guard !lastNameTextField.isFirstResponder else{
            lastNameTextField.resignFirstResponder()
            return
        }
    }

}

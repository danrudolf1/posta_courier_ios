//
//  TermsVC.swift
//  PostaCourier
//
//  Created by Dan Rudolf on 7/13/18.
//  Copyright © 2018 com.rudolfmedia. All rights reserved.
//

import UIKit

class TermsVC: UIViewController {

    @IBOutlet weak var titleBarView: UIView!
    @IBOutlet weak var termsTextView: UITextView!

    let postaTheme = PostaTheme()

    override func viewDidLoad() {
        super.viewDidLoad()
        postaTheme.applyDropShadowForTitleBar(view: titleBarView)
        termsTextView.text = Terms().termsString
        termsTextView.contentInset = UIEdgeInsets.init(top: 8, left: 8, bottom: 8, right: 8)
    }

    //MARK: Actions and Selectors
    @IBAction func onMenuPress(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)

    }

}

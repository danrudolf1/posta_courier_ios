//
//  HomeVC.swift
//  PostaCourier
//
//  Created by Dan Rudolf on 7/13/18.
//  Copyright © 2018 com.rudolfmedia. All rights reserved.
//

import UIKit
import Mapbox

class HomeVC: UIViewController, SocketConnectionPotocol, MGLMapViewDelegate, SnappyCardProtocol, TripSyncDelegate{

    @IBOutlet weak var switchContainer: UIView!
    @IBOutlet weak var mapContainer: UIView!
    @IBOutlet weak var topBar: UIView!
    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var inprogressView: UIView!
    
    let socketController = SocketController.sharedInstance
    let activeSwitch = ActiveSwitch(frame: CGRect(x: 0, y: 0, width: 55, height: 20))
    let userController = UserController.sharedInstace
    let tripController = TripController.sharedInstnce
    let postaTheme = PostaTheme()

    var connectivityView: SnappyCard!
    var initialLocation: CLLocationCoordinate2D!
    var mapView : MGLMapView!
    var mapController: MapController!
    var connectionStatusView: ConnectionStatusView?
    var parentVC: MainVC!

    deinit {
        tripController.removeDelegate(delegate: self)
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        mapController = MapController.init(view: mapContainer, parent: self, tracking: .following, from: .tripInProgress)
        self.mapView = mapController.mapView
        
        mapController.mapView.isUserInteractionEnabled = false

        postaTheme.applyDropShadowForTitleBar(view: topBar)
        setUpActiveSwitch(activeSwitch: self.activeSwitch)
        tripController.addDelegate(delegate: self)
        socketController.connectionDelegate = self
        postaTheme.fillHorizontalGradient(view: inprogressView)
        inprogressView.round(corners: .allCorners, radius: 5)
        postaTheme.applyDropShadowForTitleBar(view: inprogressView)
        updateInprogressView()
    }

    func didExpantCard() { }

    func didCollapseCard() { }

    override func viewDidAppear(_ animated: Bool) {
        checkTutorialAndUserTermsAgreement()
    }

    override func viewDidLayoutSubviews() {
        
        if connectionStatusView == nil {
            connectionStatusView = ConnectionStatusView.init(frame: CGRect.init(origin: CGPoint.zero, size: CGSize.init(width: self.view.frame.width, height: 90)), parent: self)
        }

        if connectivityView == nil {
            connectivityView = SnappyCard.init(parent: self, content: connectionStatusView!, verticalBound: 58)
            connectivityView.delegate = self
            self.view.addSubview(connectivityView)
            connectionStatusView!.setViewForState(state: .offline)
        }

    }

    func checkTutorialAndUserTermsAgreement(){

        guard Tutorial().shoulShowTutorial() == false else{
            showTutorial()
            return
        }

        guard let _ = UserController.currentUser.terms.agreedOn, let version =  UserController.currentUser.terms.version else{
            showTermsAgreement()
            return
        }
        guard version >= Terms().version else{
            showTermsAgreement()
            return
        }

    }

    func showTermsAgreement(){
        let t = NewTermsVC.init(nibName: "NewTermsVC", bundle: Bundle.main)
        present(t, animated: true, completion: nil)
    }

    func showTutorialSlides(){
        let t = TutorialVC.init(nibName: "TutorialVC", bundle: Bundle.main)
        present(t, animated: true, completion: nil)
    }

    func showTutorial(){
        let t = TutorialVC.init(nibName: "TutorialVC", bundle: Bundle.main)
        present(t, animated: true, completion: nil)
    }

    override var preferredStatusBarStyle: UIStatusBarStyle{
        return UIStatusBarStyle.lightContent
    }

    func didDisconnect() {
        connectionStatusView!.setViewForState(state: .offline)
        self.mapView.userTrackingMode = .none
        self.mapView.isUserInteractionEnabled = false
    }

    func didConnect() {
        connectionStatusView!.setViewForState(state: .online)
        self.mapView.userTrackingMode = .follow
        self.mapView.isUserInteractionEnabled = true
        self.tripController.getInprogressTrips()
    }

    func isReconnecting() {
        connectionStatusView!.setViewForState(state: .reconnecting)
    }

    func setLogoImageView(isConnected: Bool){
        logoImageView.image = isConnected ? UIImage.init(named: "online_logo") : UIImage.init(named: "offline_logo")
    }

    func checkOnlineStatsPermission(forState isOn: Bool) -> Bool{
        guard isOn == false else{
            return checkHasPayment()
        }
        return checkHasTrip()
    }

    func checkHasTrip() -> Bool{
        var canProceed = true
        guard self.tripController.currentTrip == nil else {
            canProceed = false
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                let alertVC = AlertVC.init(nibName: "AlertVC", bundle: Bundle.main)
                alertVC.modalPresentationStyle = .overCurrentContext
                alertVC.setAlertImage(alertType: .package)
                alertVC.setAlertTitle(title: "Delivery In Progress!")
                alertVC.setAlertMessage(body: "Please complete or cancel your current delivery before going offline.")
                self.present(alertVC, animated: false, completion: nil)
            }
            return canProceed
        }
        return canProceed
    }

    func checkHasPayment() -> Bool{
        var canProceed = true
        guard UserController.currentUser.stripeAccount != nil else {
            canProceed = false
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                let alertVC = AlertVC.init(nibName: "AlertVC", bundle: Bundle.main)
                alertVC.modalPresentationStyle = .overCurrentContext
                alertVC.setAlertImage(alertType: .payment)
                alertVC.setAlertTitle(title: "Payment Details")
                alertVC.setAlertMessage(body: "Please enter your banking info to receive payouts before going online.")
                self.present(alertVC, animated: false, completion: nil)
            }
            return canProceed
        }
        return canProceed
    }

    //MARK: Actions and selectors
    @objc func onSignInSwitched(_ sender: UISwitch) {
        guard checkOnlineStatsPermission(forState: sender.isOn) else{
            self.activeSwitch.animate()
            return
        }
        if sender.isOn{
            socketController.socket.connect()
        } else{
            socketController.socket.disconnect()
        }
    }

    @IBAction func onMenuPress(_ sender: Any) {
        guard let main = self.parent as? MainVC else{
            return
        }
        main.shouledShowMenu(show: true)
    }

    @objc func showTripDetails(){
        let des = TripInProgressVC.init(nibName: "TripInProgressVC", bundle: Bundle.main)
        self.present(des, animated: true, completion: nil)
    }

    //MARK: View Rendering

    func updateInprogressView(){
        guard tripController.currentTrip == nil else{
            inprogressView.isHidden = false
            return
        }
        inprogressView.isHidden = true
    }

    func setMapViewOnline(online: Bool){
        let alpha = online == true ? 1.0 : 0.60
        UIView.animate(withDuration: 0.3) {
            self.mapView.alpha = CGFloat(alpha)
        }
    }


    func setUpActiveSwitch(activeSwitch: ActiveSwitch){
        activeSwitch.isOn = false
        activeSwitch.onTintColor = #colorLiteral(red: 0.8577378979, green: 0.8577378979, blue: 0.8577378979, alpha: 1)
        activeSwitch.offTintColor = #colorLiteral(red: 0.7332697611, green: 0.7679590066, blue: 0.7923574271, alpha: 1)
        activeSwitch.cornerRadius = 0.5
        activeSwitch.thumbCornerRadius = 0.5
        activeSwitch.thumbSize = CGSize(width: 24, height: 24)
        activeSwitch.thumbTintColor = #colorLiteral(red: 0.1450980392, green: 0.5411764706, blue: 0.8705882353, alpha: 1)
        activeSwitch.padding = -1
        activeSwitch.animationDuration = 0.45
        self.switchContainer.addSubview(activeSwitch)
        activeSwitch.addTarget(self, action: #selector(onSignInSwitched(_:)), for: .valueChanged)
    }


    func currentTripCanceled() {
        parentVC.alertTripCancled()
    }

    func noCurrentTrip() {
        connectionStatusView?.removeTripInProgress()
        mapController.resetMap()
        updateInprogressView()
    }

    func currentTripWasDelivered() {
        let alertVC = AlertVC.init(nibName: "AlertVC", bundle: Bundle.main)
        alertVC.setAlertImage(alertType: .success)
        alertVC.setAlertTitle(title: "Delivery Complete!")
        alertVC.setAlertMessage(body: "Details for this delivery are now available in 'Delivery History'. Your payment blanace should reflect this trip shortly.")
        self.present(alertVC, animated: false, completion: nil)
    }

    func wasAssignedNewTrip() {
        connectionStatusView!.showTripInProgress(trip: tripController.currentTrip!)
        mapController.updateTripData(trip: tripController.currentTrip!)
    }

    func currentTripWasUpdated(){
        connectionStatusView!.showTripInProgress(trip: tripController.currentTrip!)
        mapController.updateTripData(trip: tripController.currentTrip!)
    }

}

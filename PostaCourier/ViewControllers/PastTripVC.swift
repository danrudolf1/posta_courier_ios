//
//  CurrentTripVC.swift
//  Posta
//
//  Created by Dan Rudolf on 7/12/18.
//  Copyright © 2018 com.rudolfmedia. All rights reserved.
//

import UIKit
import Mapbox

class PastTripVC: UIViewController, MGLMapViewDelegate, SnappyCardProtocol {

    @IBOutlet weak var mapContainer: UIView!
    @IBOutlet weak var mapBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var closeButton: UIButton!

    let formatter = Formatter()
    let postaTheme = PostaTheme()
    let pickupAnnotation = MGLPointAnnotation.init()
    let destinationAnnotation = MGLPointAnnotation.init()
    
    var currentTrip: Trip!
//    var mapView: MGLMapView!
    var cardContents: PastTripDetailsView!
    var card: SnappyCard!
    var mapController: MapController!

    
    override func viewDidLoad() {
        super.viewDidLoad()

        closeButton.layer.cornerRadius = 6
        closeButton.backgroundColor = UIColor.white
        postaTheme.applyButtonShadow(view: closeButton)
//        setupMapViewInContainer(container: mapContainer)
//        addAnnotations(trip: currentTrip)

    }

    override func viewDidLayoutSubviews() {
        if mapController == nil {
            self.mapController = MapController.init(view: mapContainer,
                                                    parent: self,
                                                    tracking: .disabled,
                                                    trip: currentTrip,
                                                    from: .tripInfo)
        }

        if cardContents == nil {
            cardContents = PastTripDetailsView.init(frame: CGRect.init(origin: CGPoint.zero, size: CGSize.init(width: self.view.frame.width, height: 520)))
            cardContents.populateDetailsForTripState(trip: currentTrip)
        }

        if card == nil {
            card = SnappyCard.init(parent: self, content: cardContents, verticalBound: 240)
            card.delegate = self
            self.view.addSubview(card)
            card.setCollapsed()
        }
    }

    func didCollapseCard() {
//        let inset = UIEdgeInsets.init(top: 140, left: 95, bottom: 340, right: 80)
//        mapView.setVisibleCoordinates([currentTrip.pickupGeo.getCoordinates(),
//                                       currentTrip.destintationGeo.getCoordinates()],
//                                      count: 2,
//                                      edgePadding: inset,
//                                      animated: true)
//        mapView.selectAnnotation(destinationAnnotation, animated: true)
    }
    
    func didExpantCard() { }

    //MARK: Data Population
//    func addAnnotations(trip: Trip){
//        mapView.setCenter(trip.pickupGeo.getCoordinates(), animated: false)
//
//        pickupAnnotation.coordinate = trip.pickupGeo.getCoordinates()
//        pickupAnnotation.title = "Pickup"
//        pickupAnnotation.subtitle = trip.pickup.address.street
//        mapView.addAnnotation(pickupAnnotation)
//
//        destinationAnnotation.coordinate = trip.destintationGeo.getCoordinates()
//        destinationAnnotation.title = "Dropoff"
//        destinationAnnotation.subtitle = trip.destination.address.street
//        mapView.addAnnotation(destinationAnnotation)
//
//    }
//
//
//    //MARK: Map rendering
//    func setupMapViewInContainer(container: UIView){
//        mapView = MGLMapView(frame: container.bounds)
//        mapView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
//        mapView.styleURL = URL.init(string: "mapbox://styles/postateam/cjoocoszp3pot2sn9cbze6qpw")
//        mapView.delegate = self
//        mapView.allowsTilting = false
//        mapView.allowsRotating = false
//        mapView.zoomLevel = 12
//        mapView.tintColor = #colorLiteral(red: 0.1330988705, green: 0.2163389623, blue: 0.3238252997, alpha: 1)
//        mapView.showsUserLocation = false
//        container.addSubview(mapView)
//    }
//
//    func mapView(_ mapView: MGLMapView, annotationCanShowCallout annotation: MGLAnnotation) -> Bool {
//        return true
//    }
//
//    func mapView(_ mapView: MGLMapView, viewFor annotation: MGLAnnotation) -> MGLAnnotationView? {
//        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: "MARKER")
//        if (annotationView == nil){
//            annotationView = MGLAnnotationView.init(frame: CGRect.init(x: 0, y: 0, width: 15, height: 15))
//            annotationView?.isEnabled = true
//            let imageView = UIImageView.init(frame: (annotationView?.frame)!)
//            imageView.contentMode = .scaleAspectFit
//            if (annotation.coordinate.latitude == currentTrip.pickupGeo.getCoordinates().latitude){
//                imageView.image = UIImage.init(named: "pickup_marker")
//            }
//            if (annotation.coordinate.latitude == currentTrip.destintationGeo.getCoordinates().latitude){
//                imageView.image = UIImage.init(named: "des_marker")
//            }
//            annotationView?.addSubview(imageView)
//        }
//        return annotationView
//    }

    //MARK: Actions and Selectors
    @IBAction func onBackPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }

}

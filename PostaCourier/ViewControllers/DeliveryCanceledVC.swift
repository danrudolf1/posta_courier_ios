//
//  DeliveryCanceledVC.swift
//  PostaCourier
//
//  Created by Dan Rudolf on 11/12/18.
//  Copyright © 2018 com.rudolfmedia. All rights reserved.
//

import UIKit

class DeliveryCanceledVC: UIViewController {

    @IBOutlet weak var alertContainer: UIView!
    @IBOutlet weak var alphaView: UIView!
    @IBOutlet weak var alertButton: UIButton!
    @IBOutlet weak var addressLabel: UILabel!
    
    var address: String!

    override func viewDidLoad() {
        super.viewDidLoad()
        setFramesTransparent(isTransparent: true)
        setDropShadows()
        roundViews()
    }

    override func viewDidAppear(_ animated: Bool) {
        self.animateIn()
    }


    func setCanceledAddress(street: String){
        self.address = street
    }

    func setFramesTransparent(isTransparent: Bool){
        self.alphaView.alpha = isTransparent ? 0.0 : 0.55
        self.alertContainer.alpha = isTransparent ? 0.0 : 1.0
    }

    func roundViews(){
        self.alertContainer.round(corners: .allCorners, radius: 8)
        self.alertButton.round(corners: .allCorners, radius: 5)
    }

    func setDropShadows(){
        alertContainer.setDropShadow(radius: 8.0, opacity: 0.45, offset: CGSize.init(width: 0, height: 1))
    }

    @IBAction func onAlertButtonPressed(_ sender: Any) {
        animateOut()
    }

    func animateOut(){
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseOut, animations: {
            self.setFramesTransparent(isTransparent: true)
        }) { (finished) in
            self.dismiss(animated: false, completion: nil)
        }
    }

    func animateIn(){
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseOut, animations: {
            self.setFramesTransparent(isTransparent: false)
        }) { (finished) in

        }
    }

}




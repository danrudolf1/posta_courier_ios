//
//  DeliveryHistoryCell.swift
//  Posta
//
//  Created by Dan Rudolf on 7/13/18.
//  Copyright © 2018 com.rudolfmedia. All rights reserved.
//

import UIKit

class DeliveryHistoryCell: UITableViewCell {

    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var costLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!

    var formatter = Formatter()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

    func setDetails(trip: Trip){
        iconImageView.image = trip.status == 4 ? UIImage.init(named: "delivered_icon_dark") : UIImage.init(named: "canceled_icon")
        dateLabel.text = formatter.getDateAndTime(str: trip.timestamps.updated)
        statusLabel.text = formatter.getElapsedTimeDistanceString(trip: trip)
        costLabel.text = formatter.getCurrencyValue(val: trip.transaction.amount.destinationAmount)
    }
}

//
//  RoundTimerView.swift
//  PostaCourier
//
//  Created by Dan Rudolf on 11/7/18.
//  Copyright © 2018 com.rudolfmedia. All rights reserved.
//

import UIKit

class RoundTimerView: UIView {

    let gradientMask = CAShapeLayer()

    override init(frame: CGRect) {
        super.init(frame: frame)
        layoutViews()
    }


    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        layoutViews()
    }

    func layoutViews(){
        let frame = CGRect.init(x: 5.0, y: 5.0, width: self.frame.width - 20, height: self.frame.height - 20)
        // Gradient Mask
        gradientMask.fillColor = UIColor.clear.cgColor
        gradientMask.strokeColor = UIColor.black.cgColor
        gradientMask.lineWidth = 10
        gradientMask.lineCap = kCALineCapRound
        gradientMask.frame = frame
        gradientMask.path = UIBezierPath(ovalIn: frame).cgPath

        // Gradient Layer
        let gradientLayer = CAGradientLayer()
        gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.0)
        gradientLayer.endPoint = CGPoint(x: 1.0, y: 1.0)

        // make sure to use .cgColor
        let purple = #colorLiteral(red: 0.5490196078, green: 0.5098039216, blue: 0.9411764706, alpha: 1).cgColor
        let teal = #colorLiteral(red: 0.2352941176, green: 0.8588235294, blue: 0.8862745098, alpha: 1).cgColor
        gradientLayer.colors = [purple, teal]
        gradientLayer.frame = self.bounds
        gradientLayer.mask = gradientMask
        let startAngle = 3 * (Double.pi / 2)
        gradientLayer.transform = CATransform3DMakeRotation(CGFloat(startAngle), 0.0, 0.0, 1.0)
        self.layer.addSublayer(gradientLayer)
    }

    func animateWithDuration(duration: Double){
        let animation = CABasicAnimation(keyPath: "strokeStart")
        animation.fromValue = 0.0
        animation.toValue = 1.0
        animation.duration = duration
        animation.fillMode = kCAFillModeForwards
        animation.isRemovedOnCompletion = false
        gradientMask.add(animation, forKey: "LineAnimation")
    }
}

//
//  HighlightButton.swift
//  PostaCourier
//
//  Created by Dan Rudolf on 10/19/18.
//  Copyright © 2018 com.rudolfmedia. All rights reserved.
//

import Foundation
import UIKit

class Highlightbutton: UIButton {

    override var isHighlighted: Bool {
        didSet {
            backgroundColor = isHighlighted ? UIColor.groupTableViewBackground : UIColor.white
        }
    }

}

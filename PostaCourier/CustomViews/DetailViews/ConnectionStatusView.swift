//
//  ConnectionStatusView.swift
//  PostaCourier
//
//  Created by Dan Rudolf on 11/1/18.
//  Copyright © 2018 com.rudolfmedia. All rights reserved.
//
import UIKit

enum Status {
    case online
    case offline
    case reconnecting
}

class ConnectionStatusView: UIView {

    @IBOutlet weak var detailsButton: UIButton!
    @IBOutlet weak var statusDot: UIView!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var statusDiscription: UILabel!
    @IBOutlet weak var nibView: UIView!
    @IBOutlet weak var detailsIndicator: UIImageView!

    var parentVC: HomeVC!
    var currentStatus: Status!
    let tripController = TripController.sharedInstnce

    init(frame: CGRect, parent: HomeVC) {
        parentVC = parent
        super.init(frame: frame)
        self.commonInit()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    private func commonInit() {
        Bundle.main.loadNibNamed("ConnectionStatusView", owner: self, options: nil)
        guard let content = nibView
            else {
                return
        }
        content.frame = self.bounds
        content.autoresizingMask = [.flexibleHeight, .flexibleWidth]

        self.addSubview(content)
    }

    override func layoutSubviews() {
        circleView(view: statusDot)
        self.detailsButton.isUserInteractionEnabled = true
        self.detailsIndicator.isHidden = true
        self.bringSubview(toFront: detailsButton)
    }

    func setViewForState(state: Status){
        self.currentStatus = state
        UIView.animate(withDuration: 0.2,
                       delay: 0.0,
                       options: .curveLinear,
                       animations: {
                        self.parentVC.logoImageView.alpha = 0
                        self.statusLabel.alpha = 0
                        self.statusDot.alpha = 0
                        self.statusDiscription.alpha = 0
        }) { (done) in
            if (done){
                self.upateStatusUI(state: self.currentStatus)
                UIView.animate(withDuration: 0.2) {
                    self.parentVC.logoImageView.alpha = 1
                    self.statusLabel.alpha = 1
                    self.statusDot.alpha = 1
                    self.statusDiscription.alpha = 1
                }
            }
        }
    }

    @IBAction func onShowTrip(_ sender: Any) {
        guard tripController.currentTrip != nil else{
             return
        }
        let details = TripInProgressVC.init(nibName: "TripInProgressVC", bundle: Bundle.main)
        parentVC.present(details, animated: true, completion: nil)
    }

    func showTripInProgress(trip: Trip){
        parentVC.updateInprogressView()
        var labelDes = "In Progress"
        switch (trip.status){
        case 2:
            labelDes = "Go to Pickup Location"
            break
        case 3:
            labelDes = "Deliver to Drop Off"
            break
        case 4:
            labelDes = "Delivered"
            break
        default:
            break
        }
        self.statusDiscription.text = labelDes
        self.detailsIndicator.isHidden = false
    }

    func removeTripInProgress(){
        self.statusDiscription.text = "No current trips"
        self.detailsIndicator.isHidden = true
    }

    func upateStatusUI(state: Status){
        parentVC.connectivityView.shouldScroll = true
        switch state {
        case .offline:
            statusLabel.text = "Offline"
            statusLabel.textColor = #colorLiteral(red: 0.6588235294, green: 0.7058823529, blue: 0.7450980392, alpha: 1)
            statusDot.backgroundColor = #colorLiteral(red: 0.6588235294, green: 0.7058823529, blue: 0.7450980392, alpha: 1)
            statusDiscription.isHidden = true
            statusDiscription.textColor = #colorLiteral(red: 0.6588235294, green: 0.7058823529, blue: 0.7450980392, alpha: 1)
            parentVC.mapView.showsUserLocation = false
            parentVC.connectivityView.setCollapsed()
            parentVC.setMapViewOnline(online: false)
            parentVC.setLogoImageView(isConnected: false)
            break

        case .online:
            statusLabel.text = "Online"
            statusDot.backgroundColor = #colorLiteral(red: 0.1568627451, green: 0.5411764706, blue: 0.8705882353, alpha: 1)
            statusLabel.textColor = #colorLiteral(red: 0.2196078431, green: 0.2705882353, blue: 0.3098039216, alpha: 1)
            statusDiscription.textColor = #colorLiteral(red: 0.2196078431, green: 0.2705882353, blue: 0.3098039216, alpha: 1)
            statusDiscription.text = "No Current Trips"
            statusDiscription.isHidden = false
            parentVC.connectivityView.setExpanded()
            parentVC.mapView.showsUserLocation = true
            parentVC.setLogoImageView(isConnected: true)
            parentVC.setMapViewOnline(online: true)
            break

        case .reconnecting:
            statusLabel.text = "Reconnecting..."
            statusDot.backgroundColor = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
            statusDiscription.isHidden = false
            statusLabel.textColor = #colorLiteral(red: 0.2196078431, green: 0.2705882353, blue: 0.3098039216, alpha: 1)
            statusDiscription.textColor = #colorLiteral(red: 0.2196078431, green: 0.2705882353, blue: 0.3098039216, alpha: 1)
            statusDiscription.text = "Connectivity weak"
            break
        }
        parentVC.connectivityView.shouldScroll = false
        guard tripController.currentTrip != nil else{
            return
        }
        self.showTripInProgress(trip: tripController.currentTrip!)
    }

    func circleView(view: UIView){
        view.layer.masksToBounds = false
        view.layer.cornerRadius = (view.frame.height/2)
    }
}

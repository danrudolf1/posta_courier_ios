//
//  ConfirmDeviceView.swift
//  Posta
//
//  Created by Dan Rudolf on 12/11/18.
//  Copyright © 2018 com.rudolfmedia. All rights reserved.
//

import UIKit

class ConfirmDeviceView: UIView, UITextFieldDelegate {
    
    @IBOutlet weak var touchInterceptView: UIView!
    @IBOutlet weak var validateButton: UIButton!
    @IBOutlet weak var textFieldSix: UITextField!
    @IBOutlet weak var textFieldFive: UITextField!
    @IBOutlet weak var textFieldFour: UITextField!
    @IBOutlet weak var textFieldThree: UITextField!
    @IBOutlet weak var textFieldTwo: UITextField!
    @IBOutlet weak var textFieldOne: UITextField!
    @IBOutlet weak var bottomContainer: UIView!
    @IBOutlet weak var middleContainer: UIView!
    @IBOutlet weak var topContainer: UIView!
    @IBOutlet var nibView: UIView!

    var parentVC: ResetPasswordVC!
    var textfieldsArray: [UITextField]!
    var confirmCode = ""

    let userController = UserController.sharedInstace

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    private func commonInit() {
        Bundle.main.loadNibNamed("ConfirmDeviceView", owner: self, options: nil)
        guard let content = nibView
            else {
                return
        }
        content.frame = self.bounds
        content.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        self.addSubview(content)

        textfieldsArray = [textFieldOne, textFieldTwo, textFieldThree, textFieldFour, textFieldFive, textFieldSix]

        configureTextFieldDelegate()
        setGestureRecognizers()
    }

    override func layoutSubviews() {
        postRenderView(view: validateButton)
    }

    //MARK: Actions and Selectors
    @IBAction func onCancelPressed(_ sender: UIButton) {
        resignResponders()
        self.parentVC.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onValidatePressed(_ sender: Any) {

        let confrimCode = getConfirmCode()

        guard !confrimCode.isEmpty else{
            return
        }

        let loadingView = LoadingViewVC.init(nibName: "LoadingViewVC", bundle: Bundle.main)
        loadingView.modalPresentationStyle = .overCurrentContext
        loadingView.presenting = parentVC
        loadingView.show()

        userController.confirmDevice(phone: parentVC.phoneNumber, deviceKey: parentVC.deviceToken, confirmCode: confrimCode) { (confrimDevice, err) in
            loadingView.dismissDelayed()
            guard err == nil else{
                self.showAlertVC(message: err!.message)
                return
            }
            self.parentVC.resetToken = confrimDevice!.resetKey!
            self.presentChangePasswordView()
        }

    }

    //MARK: Textfield Delegation
    func configureTextFieldDelegate(){
        for tf in textfieldsArray {
            tf.delegate = self
            tf.addTarget(self, action: #selector(textDidChange), for: .editingChanged)
        }
    }

    @objc func textDidChange(){
        let index = getResponderChainIndex()
        let tf = textfieldsArray[index]

        if tf.text?.count == 2{
            let char = tf.text?.last
            let newtf = textfieldsArray[index + 1]
            newtf.text = String(char!)
            newtf.becomeFirstResponder()

            if newtf == textFieldSix {
                resignResponders()
            }

            let originalText = tf.text!.prefix(1)
            tf.text = String(originalText)
            return
        }

        if tf.text?.count == 0 {
            if index == 0{
                return
            }
            textfieldsArray[index - 1].becomeFirstResponder()
        }
    }

    func getResponderChainIndex() -> Int{
        guard textFieldOne.isFirstResponder == false else{
            return 0
        }
        guard textFieldTwo.isFirstResponder == false else{
            return 1
        }
        guard textFieldThree.isFirstResponder == false else{
            return 2
        }
        guard textFieldFour.isFirstResponder == false else{
            return 3
        }
        guard textFieldFive.isFirstResponder == false else{
            return 4
        }
        guard textFieldSix.isFirstResponder == false else{
            return 5
        }
        return -1
    }

    @objc func resignResponders(){
        guard textFieldOne.isFirstResponder == false else {
            textFieldOne.resignFirstResponder()
            return
        }
        guard textFieldTwo.isFirstResponder == false else {
            textFieldTwo.resignFirstResponder()
            return
        }
        guard textFieldThree.isFirstResponder == false else {
            textFieldThree.resignFirstResponder()
            return
        }
        guard textFieldFour.isFirstResponder == false else {
            textFieldFour.resignFirstResponder()
            return
        }
        guard textFieldFive.isFirstResponder == false else {
            textFieldFive.resignFirstResponder()
            return
        }
        guard textFieldSix.isFirstResponder == false else {
            textFieldSix.resignFirstResponder()
            return
        }
    }

    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return true
    }

    //Validators
    func getConfirmCode() -> String{
        var str = ""
        for t in textfieldsArray{
            guard let char = t.text!.first else {
                let banner = BannerView.init(parent: self.parentVC)
                banner.setDurationAndLevel(duration: .short, level: .info)
                banner.show(message: "Please enter a valid 6-digit confirm code.")
                return str
            }
            str.append(char)
        }
        return str
    }

    //MARK: Gesture Recognition
    func setGestureRecognizers(){
        let editTap = UITapGestureRecognizer.init(target: self, action: #selector(refershCodeViews))
        editTap.cancelsTouchesInView = true
        touchInterceptView.addGestureRecognizer(editTap)

        let dismissTap = UITapGestureRecognizer.init(target: self, action: #selector(resignResponders))
        self.addGestureRecognizer(dismissTap)
    }

    @objc func refershCodeViews(){
        for tf in textfieldsArray{
            tf.text = ""
        }
        textFieldOne.becomeFirstResponder()
    }

    //MARK: View Management
    @objc func animateOut(){
        UIView.animate(withDuration: 0.4, delay: 0, options: .curveEaseIn, animations: {
            self.bottomContainer.center = CGPoint.init(x: -(self.bottomContainer.frame.width/2), y: self.bottomContainer.center.y)
        }) { (done) in }

        UIView.animate(withDuration: 0.4, delay: 0.15, options: .curveEaseIn, animations: {
            self.middleContainer.center = CGPoint.init(x: -(self.middleContainer.frame.width/2), y: self.middleContainer.center.y)
        }) { (done) in }

        UIView.animate(withDuration: 0.4, delay: 0.3, options: .curveEaseIn, animations: {
            self.topContainer.center = CGPoint.init(x: -(self.topContainer.frame.width/2), y: self.topContainer.center.y)
        }) { (done) in }
    }

    func postRenderView(view: UIView){
        let origin = view.frame.origin
        let width = self.frame.width - 32
        let height = view.frame.height
        view.frame = CGRect.init(origin: origin, size: CGSize.init(width: width, height: height))
        view.round(corners: .allCorners, radius: 5)
    }

    func presentChangePasswordView(){
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
            self.parentVC.showChangePassword()
        })
    }

    func showAlertVC(message: String){
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            let alertVC = AlertVC.init(nibName: "AlertVC", bundle: Bundle.main)
            alertVC.modalPresentationStyle = .overCurrentContext
            alertVC.setAlertImage(alertType: .fail)
            alertVC.setAlertTitle(title: "Could Not Complete Request")
            alertVC.setAlertMessage(body: message)
            self.parentVC.present(alertVC, animated: false, completion: nil)
        }
    }

}

//
//  LookupAccountView.swift
//  Posta
//
//  Created by Dan Rudolf on 12/11/18.
//  Copyright © 2018 com.rudolfmedia. All rights reserved.
//

import UIKit

class LookupAccountView: UIView, UITextFieldDelegate {

    @IBOutlet weak var bottomContainer: UIView!
    @IBOutlet weak var middleContainer: UIView!
    @IBOutlet weak var topContainer: UIView!
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var sendResetButton: UIButton!
    @IBOutlet var nibView: UIView!

    var parentVC: ResetPasswordVC!

    let userController = UserController.sharedInstace

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    private func commonInit() {
        Bundle.main.loadNibNamed("LookupAccountView", owner: self, options: nil)
        guard let content = nibView
            else {
                return
        }
        content.frame = self.bounds
        content.autoresizingMask = [.flexibleHeight, .flexibleWidth]

        self.addSubview(content)


        configureTextFieldDelegate()
        setGestureRecognizers()

    }

    override func layoutSubviews() {
        postRenderView(view: self.sendResetButton)
    }

    //MARK: Textfield Delegate

    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.text = ""
    }

    func configureTextFieldDelegate(){
        self.phoneTextField.delegate = self
        phoneTextField.addTarget(self, action: #selector(textDidChange), for: .editingChanged)
    }

    @objc func textDidChange(){
        if phoneTextField.text?.count == 14{
            resignResponders()
        }

        guard var phoneString = phoneTextField.text else{
            return
        }
        switch phoneString.count{
        case 4:
            let baseIndex = phoneTextField.text!.startIndex
            guard phoneString.prefix(1) != "(" else { return }
            phoneString.insert("(", at: baseIndex)
            phoneString.insert(")", at: phoneString.index(baseIndex, offsetBy: 4))
            phoneString.insert(" ", at: phoneString.index(baseIndex, offsetBy: 5))
            phoneTextField.text = phoneString
            break
        case 6:
            let baseIndex = phoneTextField.text!.startIndex
            print(phoneString.suffix(1))
            guard phoneString.suffix(1) != " " else {
                phoneString.remove(at: phoneString.index(baseIndex, offsetBy: 5))
                phoneString.remove(at: phoneString.index(baseIndex, offsetBy: 4))
                phoneString.remove(at: baseIndex)
                phoneTextField.text = phoneString
                return
            }
            break
        case 10:
            let baseIndex = phoneTextField.text!.startIndex
            guard phoneString.suffix(1) != " " else {
                phoneString.remove(at: phoneString.index(baseIndex, offsetBy: 9))
                phoneTextField.text = phoneString
                return
            }
            phoneString.insert(" ", at: phoneString.index(baseIndex, offsetBy: 9))
            phoneTextField.text = phoneString
            break
        default:
            break
        }
    }

    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return range.location < 14
    }

    //MARK: Actions and Selectors
    @IBAction func onCancelPressed(_ sender: UIButton) {
        resignResponders()
        self.parentVC.dismiss(animated: true, completion: nil)
    }

    @IBAction func onSendResetPressed(_ sender: UIButton) {

        guard checkValidPhoneNumber(phone: phoneTextField.text!) == true else {
            return
        }

        let phone = stripPhoneNumber(str: phoneTextField.text!)

        let loadingView = LoadingViewVC.init(nibName: "LoadingViewVC", bundle: Bundle.main)
        loadingView.modalPresentationStyle = .overCurrentContext
        loadingView.presenting = parentVC
        loadingView.show()

        userController.requestDeviceKey(phone: phone) { (recovery, err) in
            loadingView.dismissDelayed()
            guard err == nil else{
                self.showAlertVC(message: err!.message)
                return
            }
            self.parentVC.deviceToken = recovery!.deviceKey!
            self.parentVC.phoneNumber = phone
            self.presentConfirmView()
        }
    }

    //MARK: Validators
    func stripPhoneNumber(str: String) -> String{
        var trimed = str.trimmingCharacters(in: .whitespacesAndNewlines)
        trimed = trimed.replacingOccurrences(of: "(", with: "")
        trimed = trimed.replacingOccurrences(of: ")", with: "")
        return trimed.replacingOccurrences(of: " ", with: "")
    }

    func checkValidPhoneNumber(phone: String) -> Bool{
        let trimmed = stripPhoneNumber(str: phone)
        guard trimmed.count == 10 else{
            let banner = BannerView.init(parent: self.parentVC)
            banner.setDurationAndLevel(duration: .short, level: .info)
            banner.show(message: "Please enter a valid phone number.")
            return false
        }
        return true
    }

    //MARK: Gesture Recognition
    func setGestureRecognizers(){
        let dismissTap = UITapGestureRecognizer.init(target: self, action: #selector(resignResponders))
        self.addGestureRecognizer(dismissTap)
    }

    //MARK: View Management
    @objc func animateOut(){
        UIView.animate(withDuration: 0.4, delay: 0, options: .curveEaseIn, animations: {
            self.bottomContainer.center = CGPoint.init(x: -(self.bottomContainer.frame.width/2), y: self.bottomContainer.center.y)
        }) { (done) in }

        UIView.animate(withDuration: 0.4, delay: 0.15, options: .curveEaseIn, animations: {
            self.middleContainer.center = CGPoint.init(x: -(self.middleContainer.frame.width/2), y: self.middleContainer.center.y)
        }) { (done) in }

        UIView.animate(withDuration: 0.4, delay: 0.3, options: .curveEaseIn, animations: {
            self.topContainer.center = CGPoint.init(x: -(self.topContainer.frame.width/2), y: self.topContainer.center.y)
        }) { (done) in }
    }

    @objc func resignResponders(){
        _  = phoneTextField.canResignFirstResponder ? phoneTextField.resignFirstResponder() : nil
    }

    func postRenderView(view: UIView){
        let origin = view.frame.origin
        let width = self.frame.width - 32
        let height = view.frame.height
        view.frame = CGRect.init(origin: origin, size: CGSize.init(width: width, height: height))
        view.round(corners: .allCorners, radius: 5)
    }

    func presentConfirmView(){
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
            self.parentVC.showConfirmDevice()
        })
    }

    func showAlertVC(message: String){
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            let alertVC = AlertVC.init(nibName: "AlertVC", bundle: Bundle.main)
            alertVC.modalPresentationStyle = .overCurrentContext
            alertVC.setAlertImage(alertType: .fail)
            alertVC.setAlertTitle(title: "Could Not Complete Request")
            alertVC.setAlertMessage(body: message)
            self.parentVC.present(alertVC, animated: false, completion: nil)
        }
    }

}

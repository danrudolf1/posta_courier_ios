//
//  PastTripDetailsView.swift
//  PostaCourier
//
//  Created by Dan Rudolf on 10/10/18.
//  Copyright © 2018 com.rudolfmedia. All rights reserved.
//

import UIKit

class PastTripDetailsView: UIView {

    @IBOutlet weak var verticalSeporator: UIView!
    @IBOutlet weak var horizontalSeporator: UIView!
    @IBOutlet weak var iconImage: UIImageView!
    @IBOutlet weak var deliveredLabel: UILabel!
    @IBOutlet weak var receivedLabel: UILabel!
    @IBOutlet weak var taxLabel: UILabel!
    @IBOutlet weak var postaFeeLabel: UILabel!
    @IBOutlet weak var deliveryFeeLabel: UILabel!
    @IBOutlet weak var totalEarnedSmall: UILabel!
    @IBOutlet weak var totalEarnedLarge: UILabel!
    @IBOutlet weak var pickupNameLabel: UILabel!
    @IBOutlet weak var pickupStreetLabel: UILabel!
    @IBOutlet weak var desNameLabel: UILabel!
    @IBOutlet weak var desStreetLabel: UILabel!
    @IBOutlet weak var pickupDot: UIView!
    @IBOutlet weak var dropoffDot: UIView!
    @IBOutlet weak var deliveryIdLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var pickupTimeLabel: UILabel!
    @IBOutlet weak var dropoffTimeLabel: UILabel!
    @IBOutlet weak var earningsContainer: UIView!
    @IBOutlet var nibView: UIView!

    var trip: Trip!

    let formatter = Formatter()
    let postaTheme = PostaTheme()

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    private func commonInit() {
        Bundle.main.loadNibNamed("PastTripDetailsView", owner: self, options: nil)
        guard let content = nibView
            else {
                return
        }

        content.frame = self.bounds
        content.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        self.addSubview(content)
    }

    override func layoutSubviews() {
        postaTheme.fillHorizontalGradient(view: earningsContainer)
        earningsContainer.round(corners: .allCorners, radius: 8)

        postaTheme.fillHorizontalGradient(view: horizontalSeporator)
        postaTheme.fillVerticalGradient(view: verticalSeporator)

        pickupDot.setCircleView()
        dropoffDot.setCircleView()
    }

    func populateDetailsForTripState(trip: Trip){
        self.trip = trip
        switch trip.status {
        case 4:
            populateDelivered()
            break
        case 5:
            populateCanceled()
            break
        default:
            break
        }
    }

    func populateCanceled(){
        deliveredLabel.isHidden = true
        receivedLabel.isHidden = true
        iconImage.image = UIImage.init(named: "canceled_icon")
        
        pickupNameLabel.text = trip.pickup.name.getNameString()
        pickupStreetLabel.text = trip.pickup.address.street

        desNameLabel.text = trip.destination.name.getNameString()
        desStreetLabel.text = trip.destination.address.street

        deliveryIdLabel.text = "\(trip.tripId)"
        statusLabel.text = "\(trip.getTripStatus())"

        pickupTimeLabel.text = "Pickup"
        dropoffTimeLabel.text = "Drop off"

        dateLabel.text = formatter.getShortDate(str: trip.timestamps.created)

        let earned = formatter.getCurrencyValue(val: (trip.transaction.amount.destinationAmount))
        self.totalEarnedLarge.text = earned
        self.totalEarnedSmall.text = earned
        self.deliveryFeeLabel.text = formatter.getCurrencyValue(val: (trip.transaction.amount.total))
        self.postaFeeLabel.text = formatter.getCurrencyValue(val: (trip.transaction.amount.withheld))
        self.taxLabel.text = formatter.getCurrencyValue(val: (trip.transaction.amount.fees))
    }

    func populateDelivered(){
        pickupNameLabel.text = trip.pickup.name.getNameString()
        pickupStreetLabel.text = trip.pickup.address.street

        desNameLabel.text = trip.destination.name.getNameString()
        desStreetLabel.text = trip.destination.address.street

        deliveryIdLabel.text = "\(trip.tripId)"
        statusLabel.text = "\(trip.getTripStatus())"

        pickupTimeLabel.text = formatter.getTime(str: (trip.timestamps.received)!)
        dropoffTimeLabel.text = formatter.getTime(str: (trip.timestamps.delivered)!)

        dateLabel.text = formatter.getShortDate(str: trip.timestamps.created)

        let earned = formatter.getCurrencyValue(val: (trip.transaction.amount.destinationAmount))
        self.totalEarnedLarge.text = earned
        self.totalEarnedSmall.text = earned
        self.deliveryFeeLabel.text = formatter.getCurrencyValue(val: (trip.transaction.amount.total))
        self.postaFeeLabel.text = formatter.getCurrencyValue(val: (trip.transaction.amount.withheld))
        self.taxLabel.text = formatter.getCurrencyValue(val: (trip.transaction.amount.fees))
    }

}

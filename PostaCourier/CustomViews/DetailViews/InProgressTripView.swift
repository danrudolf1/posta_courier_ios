//
//  InProgressTripView.swift
//  PostaCourier
//
//  Created by Dan Rudolf on 11/1/18.
//  Copyright © 2018 com.rudolfmedia. All rights reserved.
//

import UIKit

class InProgressTripView: UIView, AlertControllerDelegate, OptionsControllerDelegate {

//    @IBOutlet weak var confirmButtonWidth: NSLayoutConstraint!
    @IBOutlet var nibView: UIView!
    @IBOutlet weak var tripStatusLabel: UILabel!
    @IBOutlet weak var tripNameLabel: UILabel!
    @IBOutlet weak var tripAddressLabel: UILabel!
    @IBOutlet weak var stageLabel: UILabel!
    @IBOutlet weak var instructionsLabel: UILabel!
    @IBOutlet weak var optionsButton: UIButton!
    @IBOutlet weak var continueButton: UIButton!
    @IBOutlet weak var destinationDot: UIView!

    let tripController = TripController.sharedInstnce
    
    var currentTrip: Trip!
    var parentVC: TripInProgressVC!

    init(frame: CGRect, trip: Trip, parent: TripInProgressVC) {
        parentVC = parent
        currentTrip = trip
        super.init(frame: frame)
        self.commonInit()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    private func commonInit() {
        Bundle.main.loadNibNamed("InProgressTripView", owner: self, options: nil)
        guard let content = nibView
            else {
                return
        }

        content.frame = self.bounds
        content.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        self.addSubview(content)
    }

    override func layoutSubviews() {
//        confirmButtonWidth.constant = self.frame.width - 24
        optionsButton.round(corners: .allCorners, radius: 5)
        continueButton.round(corners: .allCorners, radius: 5)
        destinationDot.setCircleView()
    }

    func populateDetailsForTripState(trip: Trip){
        self.currentTrip = trip
        switch trip.status {
        case 2:
            populatePickup()
            break
        case 3:
            populateDropOff()
            break
        default:
            break
        }
    }

    func populatePickup(){
        tripNameLabel.text = currentTrip.pickup.name.getNameString()
        tripAddressLabel.text = currentTrip.pickup.address.getStreet(type: .full)
        stageLabel.text = "Pickup"
        instructionsLabel.text = currentTrip.getInstructionsString()
        continueButton.setTitle("Package Received", for: .normal)
        self.tripStatusLabel.text = currentTrip.getTripStatus()
    }
    func populateDropOff(){
        tripNameLabel.text = currentTrip.destination.name.getNameString()
        tripAddressLabel.text = currentTrip.destination.address.getStreet(type: .full)
        stageLabel.text = "Drop Off"
        continueButton.setTitle("Package Delivered", for: .normal)
        instructionsLabel.text = currentTrip.getInstructionsString()
        self.tripStatusLabel.text = currentTrip.getTripStatus()
        self.destinationDot.backgroundColor = #colorLiteral(red: 0.368627451, green: 0.4156862745, blue: 0.9215686275, alpha: 1)
    }

    //MARK: OptionsController Delegate

    @IBAction func onOptionsButtonPressed(_ sender: Any) {
        let optionsVC = OptionsVC.init(nibName: "OptionsVC", bundle: Bundle.main)
        optionsVC.currentTrip = self.currentTrip
        optionsVC.delegate = self
        optionsVC.modalPresentationStyle = .overCurrentContext
        parentVC.present(optionsVC, animated: true, completion: nil)
    }

    func didSelectCallSender() {
        currentTrip.pickup.phone.makeCall()
    }

    func didSelectCallRecipient() {
        currentTrip.destination.phone.makeCall()
    }

    func didSelectCancelTrip(){
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
            let alertController = AlertViewController.init(nibName: "AlertViewController", bundle: Bundle.main)
            alertController.modalPresentationStyle = .overCurrentContext
            alertController.alertMessage = "Are you sure you want to remove yourself from the current Trip?"
            alertController.delegate = self
            alertController.kDelegateTrigger = "CANCEL_TRIP"
            self.parentVC.present(alertController, animated: false, completion: nil)
        }
    }


    @IBAction func onContinueButtonPresseed(_ sender: Any) {
        if (currentTrip.status == 2) {
            let alertController = AlertViewController.init(nibName: "AlertViewController", bundle: Bundle.main)
            alertController.modalPresentationStyle = .overCurrentContext
            alertController.alertMessage = "Have you received the package from \(currentTrip.pickup.name.getNameAndInitial())?"
            alertController.delegate = self
            alertController.kDelegateTrigger = "CONRFIRM_RECEIPT"
            parentVC.present(alertController, animated: false, completion: nil)
        }

        if (currentTrip.status == 3) {
            let des = ConfirmCodeVC.init(nibName: "ConfirmCodeVC", bundle: Bundle.main)
            des.currentTrip = self.currentTrip
            parentVC.present(des, animated: true, completion: nil)
        }
    }

    func didSelectCancel() { }

    func didSelectProceed(kDelegateTrigger: String) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
            if kDelegateTrigger == "CONRFIRM_RECEIPT" {
                self.updateTripStatus(tripId: self.currentTrip.id, status: 3, confirmCode: nil)
            }
            if kDelegateTrigger == "CANCEL_TRIP"{
                self.updateTripStatus(tripId: self.currentTrip.id, status: 1, confirmCode: nil)
            }
        }
    }

    func updateTripStatus(tripId: String, status: Int, confirmCode: String?){
        let loadingView = LoadingViewVC.init(nibName: "LoadingViewVC", bundle: Bundle.main)
        loadingView.modalPresentationStyle = .overCurrentContext
        parentVC.present(loadingView, animated: false, completion: nil)
        tripController.updateTripStatus(trip_id: tripId, status: status, confirmCode: confirmCode) { (trip, err) in
            loadingView.dismissDelayed()
            guard err == nil else{
                DispatchQueue.main.async {
                    let banner = BannerView.init(parent: self.parentVC)
                    banner.setDurationAndLevel(duration: .short, level: .warn)
                    banner.show(message: "Error updating trip status, Please try again.")
                }
                return
            }
            guard status != 1 else {
                self.dismissNavigationStack()
                return
            }
            self.currentTrip = trip
            self.refreshUI()
        }
    }

    func dismissNavigationStack(){
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 1) {
            self.tripController.getInprogressTrips()
            self.parentVC.dismiss(animated:true, completion: nil)
        }
    }

    func refreshUI(){
        DispatchQueue.main.async {
            self.currentTrip = self.tripController.currentTrip
            self.populateDetailsForTripState(trip: self.currentTrip)
            self.parentVC.mapController.updateTripData(trip: self.currentTrip)
        }
    }

}

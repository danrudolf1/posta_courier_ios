//
//  AlertVC.swift
//  PostaCourier
//
//  Created by Dan Rudolf on 11/12/18.
//  Copyright © 2018 com.rudolfmedia. All rights reserved.
//

import UIKit


class AlertVC: UIViewController {
    enum AlertType {
        case success
        case fail
        case payment
        case courier
        case location
        case package
    }

    @IBOutlet weak var alphaView: UIView!
    @IBOutlet weak var alertImageView: UIImageView!
    @IBOutlet weak var alertTitleLabel: UILabel!
    @IBOutlet weak var alertMessageLabel: UILabel!
    @IBOutlet weak var alertButton: UIButton!
    @IBOutlet weak var alertContainer: UIView!

    var alertType: AlertType!
    var alertTitle: String!
    var body: String!
    var displayIcon: UIImage!
    var shouldShrink: Bool = false

    override func viewDidLoad() {
        super.viewDidLoad()
        setFramesTransparent(isTransparent: true)
        setDropShadows()
        roundViews()

        alertTitleLabel.text = self.alertTitle
        alertMessageLabel.text = self.body
        alertImageView.image = self.displayIcon

        shrinkIfNecessary()
    }

    override func viewDidAppear(_ animated: Bool) {
        self.animateIn()
    }

    func setAlertImage(alertType: AlertType) {
        switch alertType {
        case .success:
            displayIcon = UIImage.init(named: "success_icon")
            break
        case .fail:
            shouldShrink = true
            displayIcon = UIImage.init(named: "fail_icon")
            break
        case .payment:
            displayIcon = UIImage.init(named: "payment_icon")
            break
        case .courier:
            displayIcon = UIImage.init(named: "couriers_icon")
            break
        case .location:
            displayIcon = UIImage.init(named: "location_icon")
            break
        case .package:
            displayIcon = UIImage.init(named: "package_icon")
            break
        }
    }

    func setAlertTitle(title: String){
        self.alertTitle = title
    }

    func setAlertMessage(body: String){
        self.body = body
    }

    func shrinkIfNecessary(){
        guard shouldShrink == true else{
            return
        }
        let newframe = CGRect.init(x: 0,
                                   y: 0,
                                   width: alertImageView.frame.width,
                                   height: alertImageView.frame.height - 5)

        let newCenter = CGPoint.init(x: alertImageView.center.x, y: alertImageView.center.y - 5)
        alertImageView.frame = newframe
        alertImageView.center = newCenter
    }

    func setFramesTransparent(isTransparent: Bool){
        self.alphaView.alpha = isTransparent ? 0.0 : 0.45
        self.alertContainer.alpha = isTransparent ? 0.0 : 1.0
    }

    func roundViews(){
        self.alertContainer.round(corners: .allCorners, radius: 8)
        self.alertButton.round(corners: .allCorners, radius: 5)
    }

    func setDropShadows(){
        alertContainer.setDropShadow(radius: 8.0, opacity: 0.45, offset: CGSize.init(width: 0, height: 1))
    }

    @IBAction func onAlertButtonPressed(_ sender: Any) {
        animateOut()
    }

    func animateOut(){
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseOut, animations: {
            self.setFramesTransparent(isTransparent: true)
        }) { (finished) in
            self.dismiss(animated: false, completion: nil)
        }
    }

    func animateIn(){
        UIView.animate(withDuration: 0.3, delay: 0, options: .curveEaseOut, animations: {
            self.setFramesTransparent(isTransparent: false)
        }) { (finished) in

        }
    }

}

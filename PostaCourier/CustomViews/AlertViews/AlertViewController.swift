//
//  AlertViewController.swift
//  PostaCourier
//
//  Created by Dan Rudolf on 11/1/18.
//  Copyright © 2018 com.rudolfmedia. All rights reserved.
//

import UIKit

protocol AlertControllerDelegate {
    func didSelectCancel()
    func didSelectProceed(kDelegateTrigger: String)
}

class AlertViewController: UIViewController {

    @IBOutlet weak var alertLabel: UILabel!
    @IBOutlet weak var alphaView: UIView!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var alertView: UIView!
    @IBOutlet weak var okButton: UIButton!

    var kDelegateTrigger: String!
    var delegate: AlertControllerDelegate?
    var alertMessage: String!

    override func viewDidLoad() {
        super.viewDidLoad()

        alertLabel.text = alertMessage

        cancelButton.round(corners: .allCorners, radius: 5)
        okButton.round(corners: .allCorners, radius: 5)
        roundViewCorners(view: alertView)
        setDropShadow(view: alertView)

        self.alphaView.alpha = 0
        self.alertView.alpha = 0
        self.alertView.center = CGPoint.init(x: self.view.center.x, y: self.view.center.y - 70)

        if kDelegateTrigger == "CANCEL_TRIP"{
            self.cancelButton.titleLabel?.text = "No"
            self.okButton.titleLabel?.text = "Yes"
        }
    }

    override func viewDidAppear(_ animated: Bool) {
        animateIn()
    }

    @IBAction func didPressProceed(_ sender: Any) {
        self.animateOut()
        self.delegate?.didSelectProceed(kDelegateTrigger: self.kDelegateTrigger)
    }

    @IBAction func didPressCancel(_ sender: Any) {
        self.animateOut()
    }

    func animateIn(){
        UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseOut, animations: {
            self.alphaView.alpha = 0.45
            self.alertView.alpha = 1
        }) { (finished) in

        }
    }

    func animateOut(){
        UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseOut, animations: {
            self.alphaView.alpha = 0
            self.alertView.alpha = 0
        }) { (finished) in
            self.dismiss(animated: false, completion: nil)
        }
    }

    func roundViewCorners(view: UIView){
        view.layer.cornerRadius = 8
        view.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMinYCorner, .layerMinXMaxYCorner, .layerMaxXMinYCorner]
    }

    func setDropShadow(view: UIView){
        view.layer.shadowColor = UIColor.black.cgColor
        view.layer.shadowOffset = CGSize(width: 0.0, height: 1.0)
        view.layer.shadowOpacity = 0.45
        view.layer.shadowRadius = 8
    }
}

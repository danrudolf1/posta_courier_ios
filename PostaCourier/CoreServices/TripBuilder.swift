//
//  TripBuilder.swift
//  Posta
//
//  Created by Dan Rudolf on 6/13/18.
//  Copyright © 2018 com.rudolfmedia. All rights reserved.
//

import UIKit
import MapboxGeocoder

class TripBuilder: NSObject {

    static let sharedInstance = TripBuilder()

    var currentTrip = Trip.init()
    var currentPickupLocation : CLLocationCoordinate2D? = nil
    var currentDropoffLocation : CLLocationCoordinate2D? = nil
    var codedPickupAddressDictionary : [AnyHashable : Any]?
    var codedDropoffAddressDictionary : [AnyHashable : Any]?
    let region = Region.init(bounds: Bounds.init())

    func setPickupAddress(addressDictionary : [AnyHashable : Any]){
        let state = addressDictionary["state"]
        UserController.currentUser.profile?.address?.state = state as? String

        let zip = addressDictionary["postalCode"] as? String
        UserController.currentUser.profile?.address?.zip = zip

        let city = addressDictionary["city"] as? String
        UserController.currentUser.profile?.address?.city = city

        var street = ""
        if let number = addressDictionary["subThoroughfare"]{
            let streetName = addressDictionary["name"] as! String
            street = "\(number) \(streetName)"
        } else{
            street = addressDictionary["street"] as! String
        }
        UserController.currentUser.profile?.address?.street = street

        let locationCord = [Double((currentPickupLocation?.longitude)!), Double((currentPickupLocation?.latitude)!)]
        UserController.currentUser.profile?.location?.coordinates = locationCord
        currentTrip.senderId = UserController.currentUser.profile?.id
    }

    func getPickupAddressLineOne() -> String{
        guard let street = UserController.currentUser.profile?.address?.street else {
            return "NA"
        }
       return "\(street)"
    }

    func getPickupAddressLineTwo() -> String{
        guard let city = UserController.currentUser.profile?.address?.city else {
            return "NA"
        }
        guard let zip = UserController.currentUser.profile?.address?.zip else {
            return "NA"
        }
        return ("\(city), \(zip)")
    }

//    func getSecondaryAddressDescription(geo : GeocodedPlacemark) -> String?{
//        var secondaryAddress = ""
//        guard let placemark = geo.addressDictionary else {
//            return secondaryAddress
//        }
//        if placemark["subThoroughfare"] != nil{
//            let city = placemark["city"] ?? ""
//            let state = placemark["state"] ?? ""
//            let zip = placemark["postalCode"] ?? ""
//            secondaryAddress = "\(city), \(state) \(zip)"
//
//        } else{
//            if let street = placemark["street"]{
//                secondaryAddress = "\(street)"
//            }
//            if let city = placemark["city"]{
//                let zip = placemark["postalCode"] ?? ""
//                secondaryAddress = "\(secondaryAddress), \(city) \(zip)"
//            }
//        }
//        return secondaryAddress
//    }
//
//    func getPirmaryAddressDescription(geo : GeocodedPlacemark) -> String?{
//        var primaryAddress = ""
//        guard let placemark = geo.addressDictionary else {
//            return primaryAddress
//        }
//        if let name = placemark["name"]{
//            primaryAddress = "\(name)"
//        }
//        if let number = placemark["subThoroughfare"]{
//            primaryAddress = "\(number) \(primaryAddress)"
//        }
//        return primaryAddress
//    }

    func setDestinationAddress(addressDictionary : [AnyHashable : Any]){
        let state = addressDictionary["state"]
        currentTrip.destination?.address?.state = state as? String

        let zip = addressDictionary["postalCode"] as? String
        currentTrip.destination?.address?.zip = zip

        let city = addressDictionary["city"] as? String
        currentTrip.destination?.address?.city = city

        var street = ""
        if let number = addressDictionary["subThoroughfare"]{
            let streetName = addressDictionary["name"] as! String
            street = "\(number) \(streetName)"
        } else{
            street = addressDictionary["street"] as! String
        }
        currentTrip.destination?.address?.street = street

        let locationCord = [Double((currentDropoffLocation?.longitude)!), Double((currentDropoffLocation?.latitude)!)]
        currentTrip.destintationGeo?.coordinates = locationCord

    }

    func getDestinationAddressLineOne() -> String{
        guard let street = currentTrip.destination?.address?.street else {
            return "NA"
        }
        return "\(street)"
    }

    func getDestinationAddressLineTwo() -> String{
        guard let city = currentTrip.destination?.address?.city else {
            return "NA"
        }
        guard let zip = currentTrip.destination?.address?.zip else {
            return "NA"
        }
        return ("\(city), \(zip)")
    }

}

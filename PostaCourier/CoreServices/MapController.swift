//
//  MapController.swift
//  PostaCourier
//
//  Created by Dan Rudolf on 10/31/18.
//  Copyright © 2018 com.rudolfmedia. All rights reserved.
//

import Foundation
import Mapbox

enum Tracking {
    case following
    case off
    case disabled
}

enum AppNaviagtionRoute {
    case tripInfo
    case tripInProgress
    case tripRequest
}

class MapController: NSObject, MGLMapViewDelegate {

    let parent: UIViewController
    let mapView: MGLMapView
    let userController = UserController.sharedInstace
    var geoController = GeoController.sharedInstance

    var isLocalObject: Bool = true
    var isTracking: Tracking
    var appNaviagtionRoute: AppNaviagtionRoute
    var visibleTrip: Trip?

    init(view: UIView, parent: UIViewController, tracking: Tracking, from: AppNaviagtionRoute){
        self.parent = parent
        self.isTracking = tracking
        self.appNaviagtionRoute = from
        mapView = MGLMapView(frame: view.bounds)
        super.init()

        mapView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        mapView.styleURL = URL.init(string: "mapbox://styles/postateam/cjoocoszp3pot2sn9cbze6qpw")
        mapView.setCenter(userController.lastLocation!, animated: false)
        mapView.allowsTilting = false
        mapView.allowsRotating = false
        mapView.zoomLevel = 15
        mapView.maximumZoomLevel = 17
        mapView.tintColor = #colorLiteral(red: 0.1450980392, green: 0.5411764706, blue: 0.8705882353, alpha: 1)
        mapView.showsUserLocation = false
        mapView.delegate = self
        setTrackingMode(mode: tracking)
        view.addSubview(mapView)
    }

    init(view: UIView, parent: UIViewController, tracking: Tracking, trip: Trip, from: AppNaviagtionRoute) {
        self.parent = parent
        self.isTracking = tracking
        self.visibleTrip = trip
        self.appNaviagtionRoute = from
        mapView = MGLMapView(frame: view.bounds)
        super.init()

        mapView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        mapView.styleURL = URL.init(string: "mapbox://styles/postateam/cjoocoszp3pot2sn9cbze6qpw")
        mapView.setCenter(userController.lastLocation!, animated: false)
        mapView.allowsTilting = false
        mapView.allowsRotating = false
        mapView.zoomLevel = 15
        mapView.maximumZoomLevel = 17
        mapView.tintColor = #colorLiteral(red: 0.1450980392, green: 0.5411764706, blue: 0.8705882353, alpha: 1)
        mapView.showsUserLocation = false
        setTrackingMode(mode: tracking)
        mapView.delegate = self
        view.addSubview(mapView)
        updateTripData(trip: visibleTrip!)
    }

    init(view: UIView, parent: UIViewController, trip: Trip, from: AppNaviagtionRoute) {
        self.parent = parent
        self.isTracking = Tracking.disabled
        self.visibleTrip = trip
        self.appNaviagtionRoute = from
        mapView = MGLMapView(frame: view.bounds)
        super.init()

        mapView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        mapView.styleURL = URL.init(string: "mapbox://styles/postateam/cjoocoszp3pot2sn9cbze6qpw")
        mapView.setCenter(userController.lastLocation!, animated: false)
        mapView.allowsTilting = false
        mapView.allowsRotating = false
        mapView.isUserInteractionEnabled = false
        mapView.zoomLevel = 15
        mapView.maximumZoomLevel = 17
        mapView.tintColor = #colorLiteral(red: 0.1450980392, green: 0.5411764706, blue: 0.8705882353, alpha: 1)
        setTrackingMode(mode: isTracking)
        mapView.delegate = self
        view.addSubview(mapView)
        updateTripData(trip: visibleTrip!)
    }

    func setTrackingMode(mode: Tracking){
        switch mode {
        case .following:
            mapView.showsUserLocation = true
            mapView.userTrackingMode = .follow
            break
        case .off:
            mapView.showsUserLocation = true
            mapView.userTrackingMode = .none
            break
        case .disabled:
            mapView.showsUserLocation = false
            mapView.userTrackingMode = .none
            break
        }
    }

    func resetMap(){
        removeAnnotations()
        setTrackingMode(mode: .following)
    }

    func updateTripData(trip: Trip){
        self.visibleTrip = trip

        removeAnnotations()

        switch appNaviagtionRoute{
        case .tripInfo:
            setTripInfoAnnotations(trip: trip)
            break
        case .tripInProgress:
            setTripInProgressAnnotations(trip: trip)
            break
        case .tripRequest:
            setTripRequestAnnotations(trip: trip)
        }

    }

    func setTripInfoAnnotations(trip: Trip){
        let pickupAnnotation = MGLPointAnnotation.init()
        pickupAnnotation.coordinate = trip.pickupGeo.getCoordinates()
        pickupAnnotation.subtitle = trip.pickup.address.street

        let destinationAnnotation = MGLPointAnnotation.init()
        destinationAnnotation.coordinate = trip.destintationGeo.getCoordinates()
        destinationAnnotation.subtitle = trip.destination.address.street

        switch (trip.status) {
        case 2:
            pickupAnnotation.title = "Pick Up"
            mapView.addAnnotation(pickupAnnotation)
            mapView.selectAnnotation(pickupAnnotation, animated: true)
            let coords = [pickupAnnotation.coordinate, geoController.lastLocation.coordinate]
            self.setVisibleCoordinates(insets: UIEdgeInsets.init(top: 150, left: 70, bottom: 150, right: 70), coordinates: coords)
            break
            
        case 3:
            destinationAnnotation.title = "Drop Off"
            mapView.addAnnotation(destinationAnnotation)
            mapView.selectAnnotation(destinationAnnotation, animated: true)
            let coords = [destinationAnnotation.coordinate, geoController.lastLocation.coordinate]
            self.setVisibleCoordinates(insets: UIEdgeInsets.init(top: 150, left: 70, bottom: 150, right: 70), coordinates: coords)
            break

        case 4:
            pickupAnnotation.title = "Pick Up"
            mapView.addAnnotation(pickupAnnotation)

            destinationAnnotation.title = "Drop Off"
            mapView.addAnnotation(destinationAnnotation)
            mapView.selectAnnotation(destinationAnnotation, animated: true)
            mapView.setCenter(destinationAnnotation.coordinate, animated: true)
            break
        case 5:
            pickupAnnotation.title = "Canceled"
            mapView.addAnnotation(pickupAnnotation)
            mapView.selectAnnotation(pickupAnnotation, animated: true)
            mapView.setCenter(pickupAnnotation.coordinate, animated: true)
            break
        default:
            break
        }

    }

    func setTripInProgressAnnotations(trip: Trip){
        let pickupAnnotation = MGLPointAnnotation.init()
        pickupAnnotation.coordinate = trip.pickupGeo.getCoordinates()
        pickupAnnotation.subtitle = trip.pickup.address.street

        let destinationAnnotation = MGLPointAnnotation.init()
        destinationAnnotation.coordinate = trip.destintationGeo.getCoordinates()
        destinationAnnotation.subtitle = trip.destination.address.street

        switch (trip.status) {
        case 2:
            pickupAnnotation.title = "Pick Up"
            mapView.addAnnotation(pickupAnnotation)
            mapView.selectAnnotation(pickupAnnotation, animated: true)
            let coords = [pickupAnnotation.coordinate, geoController.lastLocation.coordinate]
            self.setVisibleCoordinates(insets: UIEdgeInsets.init(top: 150, left: 70, bottom: 150, right: 70), coordinates: coords)
            break
        case 3:
            destinationAnnotation.title = "Drop Off"
            mapView.addAnnotation(destinationAnnotation)
            mapView.selectAnnotation(destinationAnnotation, animated: true)
            let coords = [destinationAnnotation.coordinate, geoController.lastLocation.coordinate]
            self.setVisibleCoordinates(insets: UIEdgeInsets.init(top: 150, left: 70, bottom: 150, right: 70), coordinates: coords)
            break
        case 4:
            pickupAnnotation.title = "Pick Up"
            mapView.addAnnotation(pickupAnnotation)

            destinationAnnotation.title = "Drop Off"
            mapView.addAnnotation(destinationAnnotation)
            mapView.selectAnnotation(destinationAnnotation, animated: true)
            mapView.setCenter(destinationAnnotation.coordinate, animated: true)
            break
        case 5:
            pickupAnnotation.title = "Canceled"
            mapView.addAnnotation(pickupAnnotation)
            mapView.selectAnnotation(pickupAnnotation, animated: true)
            mapView.setCenter(pickupAnnotation.coordinate, animated: true)
            break
        default:
            break
        }

    }

    func setTripRequestAnnotations(trip: Trip){
        let requestAnnotation = MGLPointAnnotation.init()
        requestAnnotation.coordinate = trip.pickupGeo.getCoordinates()
        mapView.addAnnotation(requestAnnotation)
        mapView.setCenter(requestAnnotation.coordinate, animated: true)
    }


    func removeAnnotations(){
        if mapView.annotations != nil {
            for a in mapView.annotations!{
                if !(a is MGLUserLocation){
                    mapView.removeAnnotation(a)
                }
            }
        }
    }


    func mapView(_ mapView: MGLMapView, annotationCanShowCallout annotation: MGLAnnotation) -> Bool {
        return true
    }

    func mapView(_ mapView: MGLMapView, viewFor annotation: MGLAnnotation) -> MGLAnnotationView? {

        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: "MARKER")

        guard !(annotation is MGLUserLocation) else{
            return nil
        }

        if (annotationView == nil){
            annotationView = getViewForAnnotation(annotation: annotation)
        }
        return annotationView
    }

    func getViewForAnnotation(annotation: MGLAnnotation) -> MGLAnnotationView{
        
        let annotationView = MGLAnnotationView.init(frame: CGRect.init(x: 0, y: 0, width: 20, height: 20))
        annotationView.isEnabled = true

        let imageView = UIImageView.init(frame: annotationView.frame)

        switch (annotation.coordinate){
        case visibleTrip!.destintationGeo.getCoordinates():
            imageView.image = UIImage.init(named: "des_marker")
            break

        case visibleTrip!.pickupGeo.getCoordinates():
            imageView.image = appNaviagtionRoute == AppNaviagtionRoute.tripRequest ? UIImage.init(named: "package_icon") : UIImage.init(named: "pickup_marker")
            break

        default:
            break
        }

        annotationView.addSubview(imageView)
        return annotationView
    }

    func setVisibleCoordinates(insets: UIEdgeInsets, coordinates: [CLLocationCoordinate2D]){
        let count = UInt.init(bitPattern: coordinates.count)
        mapView.setVisibleCoordinates(coordinates,
                                      count: count,
                                      edgePadding: insets,
                                      animated: true)

    }


}

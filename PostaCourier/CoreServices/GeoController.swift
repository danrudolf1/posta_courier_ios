//
//  GeoController.swift
//  PostaCourier
//
//  Created by Dan Rudolf on 10/31/18.
//  Copyright © 2018 com.rudolfmedia. All rights reserved.
//

import UIKit
import CoreLocation

protocol CourierLocationDelegate {
    func didUpdateCourierLocation(location: CLLocation)
}

class GeoController: NSObject, CLLocationManagerDelegate {

    static let sharedInstance = GeoController()
    let manager = CLLocationManager()

    var locationDelegate: CourierLocationDelegate?
    var lastLocation: CLLocation! {
        didSet{
            lastSet = Date.init()
        }
    }
    var lastSet: Date = Date.init()

    //LocationManger
    func locate(completionHandler: @escaping (CLLocation?) -> Void){
        startUpdatingLocation()
        DispatchQueue.main.asyncAfter(deadline: .now() + 2.5) {
            self.stoptUpdatingLocation()
            completionHandler(self.lastLocation)
        }
    }

    func startUpdatingLocation(){
        manager.requestWhenInUseAuthorization()
        manager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        manager.pausesLocationUpdatesAutomatically = true
        manager.allowsBackgroundLocationUpdates = true
        manager.delegate = self
        manager.startUpdatingLocation()
        print("Did begin updates")
    }

    func stoptUpdatingLocation(){
        manager.stopUpdatingLocation()
        print("Did end location updates")
    }

    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard lastLocation != nil else {
            self.lastLocation = locations.last
            self.locationDelegate?.didUpdateCourierLocation(location: self.lastLocation)
            return
        }
        let distance = Double(locations.last!.distance(from: lastLocation))
        if distance > 10.0 {
            self.lastLocation = locations.last!
            self.locationDelegate?.didUpdateCourierLocation(location: self.lastLocation)
        }
    }



    
}

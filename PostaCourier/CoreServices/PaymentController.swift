//
//  PaymentController.swift
//  PostaCourier
//
//  Created by Dan Rudolf on 10/6/18.
//  Copyright © 2018 com.rudolfmedia. All rights reserved.
//

import Foundation

class PaymentController: NSObject {

    let environment = Env.production
    

    func getStripeDashLink(completionHandler: @escaping (URL?, Err?) -> Void) {
        let restService = RestService.init(environment: environment)
        restService.setMethod(method: restService.methods.GET)
        restService.setPath(pathString: restService.routes.get.stripeLinkV1)
        restService.appendPathParams(params: UserController.currentUser.id)
        let req = restService.buildRequest()
        let session = URLSession.shared
        _ = session.dataTask(with: req) { (data, response, error) in
            let taskResponse = restService.parseDataTaskResponse(data: data, response: response, err: error)
            if taskResponse is Err {
                completionHandler(nil, taskResponse as? Err)
                return
            }
            let serializerResponse = restService.coder.decodeJSON(responseData: taskResponse as! Data, formatStruct: UserLinkRes.self)
            if serializerResponse is Err {
                completionHandler(nil, serializerResponse as? Err)
                print(serializerResponse)
                return
            }
            let userLinkRes = serializerResponse as! UserLinkRes
            let url = URL.init(string: userLinkRes.link!)
            completionHandler(url, nil)
            }.resume()
    }
    
}

struct UserLinkRes : Codable {
    let success : Bool
    let error : Err?
    let link : String?
}


//
//  TripController.swift
//  Posta
//
//  Created by Dan Rudolf on 7/6/18.
//  Copyright © 2018 com.rudolfmedia. All rights reserved.
//

import UIKit

protocol TripSyncDelegate {
    func currentTripCanceled()
    func currentTripWasDelivered()
    func currentTripWasUpdated()
    func noCurrentTrip()
    func wasAssignedNewTrip()
}

class TripController: NSObject {

    static let sharedInstnce = TripController()
    let environment = Env.production
    
    var currentTrip: Trip?
    var tripSyncDelegates = [TripSyncDelegate]()

    func clean(){
        currentTrip = nil
        tripSyncDelegates.removeAll()
    }

    func broadcastCurrentTripWasDelivered(){
        DispatchQueue.main.async {
            self.tripSyncDelegates.forEach({ (delegate) in
                delegate.currentTripWasDelivered()
            })
        }
    }

    func broadcastNoCurrentTrip(){
        DispatchQueue.main.async {
            self.tripSyncDelegates.forEach({ (delegate) in
                delegate.noCurrentTrip()
            })
        }
    }

    func broadcastCurrentTripWasCanceled(){
        DispatchQueue.main.async {
            self.tripSyncDelegates.forEach({ (delegate) in
                delegate.currentTripCanceled()
            })
        }
    }

    func broadcastCurrentTripWasUpdated(){
        DispatchQueue.main.async {
            self.tripSyncDelegates.forEach({ (delegate) in
                delegate.currentTripWasUpdated()
            })
        }
    }

    func broadcastWasAssignedNewTrip(){
        DispatchQueue.main.async {
            self.tripSyncDelegates.forEach({ (delegate) in
                delegate.wasAssignedNewTrip()
            })
        }
    }

    func addDelegate(delegate: AnyObject?){
        tripSyncDelegates.append(delegate as! TripSyncDelegate)
    }

    func removeDelegate(delegate: AnyObject){
        tripSyncDelegates = tripSyncDelegates.filter { type(of: $0) != type(of: delegate) }
    }

    func updateTripStatus(trip_id: String, status: Int, confirmCode: String?, completionHandler: @escaping (Trip?, Err?) -> Void) {
        let restService = RestService.init(environment: environment)
        let pathParams = trip_id
        var body: [AnyHashable: Any] = ["status": status]
        if let code = confirmCode {
            body["confirm_code"] = code
        }
        restService.setBody(dictionary: body)
        restService.setMethod(method: restService.methods.PUT)
        restService.setPath(pathString: restService.routes.put.updateStatusV1)
        restService.appendPathParams(params: pathParams)
        let req = restService.buildRequest()
        let session = URLSession.shared
        _ = session.dataTask(with: req) { (data, response, error) in
            let taskResponse = restService.parseDataTaskResponse(data: data, response: response, err: error)
            if taskResponse is Err {
                completionHandler(nil, taskResponse as? Err)
                return
            }
            let serializerResponse = restService.coder.decodeJSON(responseData: taskResponse as! Data, formatStruct: TripRes.self)

            if serializerResponse is Err {
                completionHandler(nil, serializerResponse as? Err)
                return
            }

            let tripRes = serializerResponse as! TripRes
            guard tripRes.trip?.status != 4 else{
                self.currentTrip = nil
                self.broadcastCurrentTripWasDelivered()
                completionHandler(tripRes.trip, nil)
                return
            }
            self.currentTrip = tripRes.trip
            self.broadcastCurrentTripWasUpdated()
            completionHandler(tripRes.trip, nil)
            }.resume()
    }

    func getCompletedTrips(completionHandler: @escaping ([Trip]?, Err?) -> Void) {
        let restService = RestService.init(environment: environment)
        let queryParams = ["current": "false", "courier": "true"]
        let pathParams = UserController.currentUser.profile!.id!
        restService.setMethod(method: restService.methods.GET)
        restService.setPath(pathString: restService.routes.get.tripsV1)
        restService.appendPathParams(params: pathParams)
        restService.appendQueryParams(params: queryParams)
        let req = restService.buildRequest()
        let session = URLSession.shared
        _ = session.dataTask(with: req) { (data, response, error) in
            let taskResponse = restService.parseDataTaskResponse(data: data, response: response, err: error)
            if taskResponse is Err {
                completionHandler(nil, taskResponse as? Err)
                return
            }
            let serializerResponse = restService.coder.decodeJSON(responseData: taskResponse as! Data, formatStruct: TripArrayRes.self)
            if serializerResponse is Err {
                completionHandler(nil, serializerResponse as? Err)
                return
            }
            let tripRes = serializerResponse as! TripArrayRes
            completionHandler(tripRes.trips, nil)
            }.resume()
    }

    func getInprogressTrips() {
        let restService = RestService.init(environment: environment)
        let queryParams = ["current": "true", "courier": "true"]
        let pathParams = UserController.currentUser.profile!.id!
        restService.setMethod(method: restService.methods.GET)
        restService.setPath(pathString: restService.routes.get.tripsV1)
        restService.appendPathParams(params: pathParams)
        restService.appendQueryParams(params: queryParams)
        let req = restService.buildRequest()
        let session = restService.getDataTaskSession(timeout: .defaultDuration)
        _ = session.dataTask(with: req) { (data, response, error) in
            let taskResponse = restService.parseDataTaskResponse(data: data, response: response, err: error)
            if taskResponse is Err {
                return
            }
            let serializerResponse = restService.coder.decodeJSON(responseData: taskResponse as! Data, formatStruct: TripArrayRes.self)
            if serializerResponse is Err {
                return
            }
            let tripRes = serializerResponse as! TripArrayRes
            guard let trip = tripRes.trips?.first else{
                self.currentTrip = nil
                self.broadcastNoCurrentTrip()
                return
            }
            self.currentTrip = trip
            self.broadcastWasAssignedNewTrip()
            }.resume()
    }

}

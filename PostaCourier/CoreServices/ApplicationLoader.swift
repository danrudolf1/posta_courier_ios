//
//  ApplicationLoader.swift
//  PostaCourier
//
//  Created by Dan Rudolf on 10/31/18.
//  Copyright © 2018 com.rudolfmedia. All rights reserved.
//
//
//  ApplicationLoader.swift
//  Posta
//
//  Created by Dan Rudolf on 10/24/18.
//  Copyright © 2018 com.rudolfmedia. All rights reserved.
//

import UIKit
import CoreLocation
import Lottie

class ApplicationLoader: UIViewController {

    let authController = AuthController.sharedInstance
    let userController = UserController.sharedInstace
    let geoController = GeoController.sharedInstance

    @IBOutlet weak var loadingView: UIView!

    override func viewDidAppear(_ animated: Bool) {

        guard NetworkProbe.isConnectedToNetwork() else {
            let banner = BannerView.init(parent: self)
            banner.setDurationAndLevel(duration: .medium, level: .info)
            banner.show(message: "This device is not connected to the internet.")
            DispatchQueue.main.asyncAfter(deadline: .now() + 4.0) {
                self.presentLogin()
            }
            return
        }
        checkReturning()

        let animationView = AnimationView(name: "loading_animation")
        animationView.frame = loadingView.bounds
        self.loadingView.addSubview(animationView)
        animationView.loopMode = .loop
        animationView.play()
    }

    //Login User
    func checkReturning(){
        guard let username = authController.getCurrentUserName() else{
            self.presentLogin()
            return
        }
        guard let password = authController.getCurrentPassword() else{
            self.presentLogin()
            return
        }
        loginUser(username: username, password: password)
    }

    func loginUser(username: String, password: String){
        userController.loginUser(user: username, password: password) { (user, err) in
            guard err == nil else{
                self.presentLogin()
                return
            }

            UserController.currentUser = user!
            self.authController.setNewUsername(username: username)
            self.authController.setNewPassword(password: password)
            self.locateUser()
        }
    }

    // Ready application Data
    func locateUser(){

        let locationAuthorizationStatus = CLLocationManager.authorizationStatus()
        switch locationAuthorizationStatus {
        case .notDetermined:
            break
        case .authorizedWhenInUse, .authorizedAlways:
            break
        case .restricted, .denied:
            self.alertLocationAccessNeeded()
            return
        }

        geoController.locate { (location) in
            guard let usrLocation = location else{
                AuthController.sharedInstance.logout()
                self.presentLogin()
                return
            }
            self.userController.lastLocation = usrLocation.coordinate
            self.presentApplication()
        }
    }

    func alertLocationAccessNeeded() {

        DispatchQueue.main.async {
            let alertVC = AlertVC.init(nibName: "AlertVC", bundle: Bundle.main)
            alertVC.modalPresentationStyle = .overCurrentContext
            alertVC.setAlertImage(alertType: .location)
            alertVC.setAlertTitle(title: "Location Services Disabled")
            alertVC.setAlertMessage(body: "Posta needs to access your location in order to work properly, please enable location services in the settings menu.")
            self.present(alertVC, animated: false, completion: nil)
            AuthController.sharedInstance.logout()
            self.presentLogin()
        }
    }

    //Navigation Actions
    func presentApplication(){
        DispatchQueue.main.async {
            AppDelegate.shared.applicationStateController.presentApplicationRoot()
        }
    }

    func presentLogin(){
        DispatchQueue.main.async {
            AppDelegate.shared.applicationStateController.presentLogin()
        }
    }

}


//
//  SocketController.swift
//  PostaCourier
//
//  Created by Dan Rudolf on 7/13/18.
//  Copyright © 2018 com.rudolfmedia. All rights reserved.
//

import UIKit
import SocketIO
import CoreLocation



private struct SocketEvents {
    let register = "registerSocket"
    let startUpdating = "startUpdatingLocation"
    let publishLocation = "publishLocationUpdate"
    let notifyTrip = "notifyTrip"
    let pushTrip = "pushTrip"
    let cancelTrip = "cancelTrip"
}

protocol SocketConnectionPotocol {
    func didConnect()
    func didDisconnect()
    func isReconnecting()
}

protocol SocketDataProtocol {
    func didReceiveNewTripRequest(trip: Trip)
}

private struct Host {
    let testLocal = URL.init(string: "http://10.1.10.18:3001")!
    let testRemote = URL.init(string: "http://35.202.245.42:1337")!
    let production = URL.init(string: "https://posta.systems")!
}

class SocketController: NSObject, CourierLocationDelegate {

    fileprivate let events = SocketEvents()
    static let sharedInstance = SocketController.init(environment: .production)

    let tripController = TripController.sharedInstnce
    let geoController = GeoController.sharedInstance
    let jsonCoder = JSONCoder()
    let manager: SocketManager!

    var socket: SocketIOClient!
    var seconds = 58
    var timer = Timer()

    var shouldPublishLocationUpdates = false {
        didSet{
            switch shouldPublishLocationUpdates {
            case true:
                geoController.locationDelegate = self
                geoController.startUpdatingLocation()
                break
            case false:
                geoController.locationDelegate = nil
                geoController.stoptUpdatingLocation()
                break
            }
        }
    }


    var connectionDelegate: SocketConnectionPotocol?
    var dataDelegate: SocketDataProtocol?
    var ackEmitter: SocketAckEmitter?

    init(environment: Env) {
        var url: URL
        switch (environment ){
        case .production:
            url = Host().production
            break
        case .testLocal:
            url = Host().testLocal
            break
        case .testRemote:
            url = Host().testRemote
            break
        }
        manager = SocketManager(socketURL: url, config: [.log(false), .compress])

        super.init()

        geoController.locationDelegate = self
        self.setupSocket()
    }

    func setupSocket(){
        self.socket = manager.socket(forNamespace: "/courier-sockets")
        manager.reconnects = true
        manager.reconnectWait = 5
        manager.forceNew = true
        self.listenForLocationRequest()
        self.listenForNewTrips()
        self.listenForAcecptedTrips()
        self.listenForCanceledTrip()

        socket.on(clientEvent: .connect) {data, ack in
            self.shouldPublishLocationUpdates = true
            self.registerSocket()
        }

        socket.on(clientEvent: .disconnect) { (data, ack) in
            self.shouldPublishLocationUpdates = false
            self.connectionDelegate?.didDisconnect()
        }
        socket.on(clientEvent: .reconnect) { (data, ack) in
            self.connectionDelegate?.isReconnecting()
        }
    }

    func registerSocket() {
        let data = ["id" : UserController.currentUser.profile?.id]
        socket.emit(events.register, data)
    }

    func didUpdateCourierLocation(location: CLLocation) {
        guard shouldPublishLocationUpdates == true else {
            return
        }
        let data = ["id" : UserController.currentUser.profile!.id as Any,
                    "lat" : location.coordinate.latitude,
                    "lon" : location.coordinate.longitude] as [String : Any]
        socket.emit(events.publishLocation, data)
    }

    func listenForLocationRequest(){
        socket.on(events.startUpdating) { (data, ack) in
            var res = data[0] as! [String : Any]
            guard let proceed = res["proceed"] as? Bool else { return }
            if (proceed == true){
                self.shouldPublishLocationUpdates = true
                self.connectionDelegate?.didConnect()
                self.emitInitialCoordinate()
            }
        }
    }

    func emitInitialCoordinate(){
        self.didUpdateCourierLocation(location: geoController.lastLocation)
    }

    func listenForNewTrips(){
        socket.on(events.notifyTrip) { (data, ack) in
            guard let resString = data[0] as? String else{
                return
            }
            let serializerResponse = self.jsonCoder.decodeSocketJSON(responseString: resString, formatStruct: Trip.self)
            if serializerResponse is Err {
                print(serializerResponse)
                return
            }
            let tripRes = serializerResponse as! Trip
            self.ackEmitter = ack
            self.beginMonitoringTimeout()

            print("received new trip")

            self.dataDelegate?.didReceiveNewTripRequest(trip: tripRes)
        }
    }

    func listenForAcecptedTrips(){
        socket.on(events.pushTrip) { (data, ack) in
            guard let resString = data[0] as? String else{
                return
            }
            let serializerResponse = self.jsonCoder.decodeSocketJSON(responseString: resString, formatStruct: Trip.self)
            if serializerResponse is Err {
                print(serializerResponse)
                return
            }
            let tripRes = serializerResponse as! Trip
            self.ackEmitter = ack
            self.tripController.currentTrip = tripRes
            self.tripController.broadcastWasAssignedNewTrip()
        }
    }

    func listenForCanceledTrip(){
        socket.on(events.cancelTrip) { (data, ack) in
            guard let resString = data[0] as? String else{
                return
            }
            let serializerResponse = self.jsonCoder.decodeSocketJSON(responseString: resString, formatStruct: Trip.self)
            if serializerResponse is Err {
                print(serializerResponse)
                return
            }
            let tripRes = serializerResponse as! Trip
            self.tripController.currentTrip = tripRes

            let delegate = UIApplication.shared.delegate as? AppDelegate
            delegate?.triggerCanceledTripDelegate(canceled: tripRes)
            self.tripController.broadcastCurrentTripWasCanceled()
        }
    }



    func acceptTrip(){
        guard self.ackEmitter != nil else{
            return
        }
        self.ackEmitter!.with(["accepted" : true])
        self.ackEmitter = nil
        self.cancelTimeout()
    }

    func declineTrip(){
        guard self.ackEmitter != nil else{
            return
        }
        self.ackEmitter!.with(["accepted" : false])
        self.ackEmitter = nil
        self.cancelTimeout()
    }

    //MARK: Timer
    func beginMonitoringTimeout() {
        seconds = 58
        timer = Timer.scheduledTimer(timeInterval: 1, target: self,   selector: #selector(updateTimer), userInfo: nil, repeats: true)
    }

    @objc func updateTimer() {
        if seconds < 1 {
            timer.invalidate()
            executeTimeout()
        } else {
            seconds -= 1
        }
    }

    func executeTimeout(){
        NotificationCenter.default.post(Notification.init(name: .tripDidTimeout))
    }

    func cancelTimeout(){
        timer.invalidate()
        seconds = 58
    }

}

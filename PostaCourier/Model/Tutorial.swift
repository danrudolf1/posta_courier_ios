//
//  Tutorial.swift
//  PostaCourier
//
//  Created by Dan Rudolf on 8/26/19.
//  Copyright © 2019 com.rudolfmedia. All rights reserved.
//

import UIKit

class Tutorial: NSObject {

    let authController = AuthController.sharedInstance
    let currentVersion = 1

    func shoulShowTutorial() -> Bool{
        return authController.shouldShowTutorial(version: currentVersion)
    }

    func setTutorialCompleted(){
        authController.setTutorialCompleted(completedVersion: currentVersion)
    }
}

//
//  Transaction.swift
//  PostaCourier
//
//  Created by Dan Rudolf on 10/12/18.
//  Copyright © 2018 com.rudolfmedia. All rights reserved.
//

import Foundation

struct Transaction : Codable {
    var id: String
    var amount: Amount

    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case amount
    }
}

struct Amount : Codable {
    var total: Int
    var fees: Int
    var destinationAmount: Int
    var withheld: Int

    enum CodingKeys: String, CodingKey {
        case total = "total_charge"
        case fees = "total_fees"
        case destinationAmount = "destination_amount"
        case withheld = "withheld"
    }
}

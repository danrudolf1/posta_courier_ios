//
//  Region.swift
//  Posta
//
//  Created by Dan Rudolf on 6/21/18.
//  Copyright © 2018 com.rudolfmedia. All rights reserved.
//

import Foundation

struct RegionRes : Codable {
    let success : Bool
    let error : Err?
    let region : Region?
}

struct Region : Codable{
    let bounds: Bounds
}

struct Bounds : Codable{
    let maxLat: Double = 41.9014279
    let maxLon: Double = -87.6544101
    let minLat: Double = 41.869939
    let minLon: Double = -87.6212365
    enum CodingKeys: String, CodingKey {
        case maxLat = "max_lat"
        case maxLon = "max_lon"
        case minLat = "min_lat"
        case minLon = "min_lon"
    }
}

//
//  Trip.swift
//  Posta
//
//  Created by Dan Rudolf on 6/7/18.
//  Copyright © 2018 com.rudolfmedia. All rights reserved.
//

import Foundation
import CoreLocation

struct TripArrayRes : Codable {
    let success : Bool
    let error : Err?
    let trips : [Trip]?
}

struct TripRes : Codable {
    let success : Bool
    let error : Err?
    let trip : Trip?
}

struct Trip: Codable {
    var id: String
    var tripId: String
    var status: Int
    var confirmCode: Int?
    var instructions: String?
    var pickup: Pickup
    var destination: Destination
    var destintationGeo = DestinationGeo()
    var transaction: Transaction
    var pickupGeo = PickupGeo()
    var timestamps: TimeStamps
    var description: String?
    var courier: Courier?

    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case tripId = "trip_id"
        case status
        case confirmCode = "confirm_code"
        case instructions
        case destintationGeo = "destination_geo"
        case pickupGeo = "pickup_geo"
        case pickup
        case destination
        case description
        case timestamps = "timestamps"
        case transaction
        case courier
    }

}

struct TripRequest: Codable {
    var status: Int?
    var senderId: String
    var instructions: String?
    var destination: RequestedDestination?
    var destintationGeo : DestinationGeo?
    var description: String?

    enum CodingKeys: String, CodingKey {
        case status
        case senderId = "sender_id"
        case instructions
        case destintationGeo = "destination_geo"
        case destination
        case description
    }

    init(senderId: String) {
        self.senderId = senderId
    }
}

extension Trip {
    func getInstructionsString() -> String {
        if (self.instructions != nil && self.instructions?.isEmpty == false){
            return self.instructions!
        } else{
            return "None"
        }
    }
}

struct DestinationGeo: Codable {
    var coordinates: Array<Double>?
}
extension DestinationGeo {
    func getCoordinates() -> CLLocationCoordinate2D{
        return CLLocationCoordinate2D.init(latitude: self.coordinates![1], longitude: self.coordinates![0])
    }
    mutating func setCoordinnates(coordinates: CLLocationCoordinate2D){
        let locationCord = [Double(coordinates.longitude), Double(coordinates.latitude)]
        self.coordinates = locationCord
    }
}

struct PickupGeo: Codable {
    var coordinates: Array<Double>?
}
extension PickupGeo {
    func getCoordinates() -> CLLocationCoordinate2D{
        return CLLocationCoordinate2D.init(latitude: self.coordinates![1], longitude: self.coordinates![0])
    }
    mutating func setCoordinnates(coordinates: CLLocationCoordinate2D){
        let locationCord = [Double(coordinates.longitude), Double(coordinates.latitude)]
        self.coordinates = locationCord
    }
}

struct Pickup: Codable {
    var name: Name
    var phone: String
    var address: Address
    enum CodingKeys: String, CodingKey {
        case name
        case phone
        case address
    }
}

struct Destination: Codable {
    var name: Name
    var phone: String
    var address: Address
    enum CodingKeys: String, CodingKey {
        case name
        case phone
        case address
    }
}

struct RequestedDestination: Codable {
    var name: Name?
    var phone: String?
    var address: Address
    enum CodingKeys: String, CodingKey {
        case name
        case phone
        case address
    }
}

extension Trip {
    func getTripStatus() -> String{
        switch self.status {
        case 0:
            return "Confirming Address"
        case 1:
            return "Contacting Couriers"
        case 2:
            return "Pickup In Progress"
        case 3:
            return "Out for Delivery"
        case 4:
            return "Delivered"
        case 5:
            return "Canceled"
        default:
            return "NA"
        }
    }
}

struct TimeStamps : Codable {
    let created: String
    let updated: String
    let confirmed: String?
    let accepted: String?
    let received: String?
    let delivered: String?
}




//
//  User.swift
//  Posta
//
//  Created by Dan Rudolf on 6/13/18.
//  Copyright © 2018 com.rudolfmedia. All rights reserved.
//

import Foundation

struct RecoveryRes : Codable {
    let success : Bool
    let error : Err?
    let deviceKey : String?

    enum CodingKeys: String, CodingKey {
        case deviceKey = "device_key"
        case success
        case error
    }
}

struct ConfrimDeviceRes : Codable {
    let success : Bool
    let error : Err?
    let resetKey : String?

    enum CodingKeys: String, CodingKey {
        case resetKey = "device_confirmed"
        case success
        case error
    }
}

struct PasswordResetRes : Codable {
    let success : Bool
    let error : Err?
    let password_updated : Bool?
}

struct UserRes : Codable {
    let success : Bool
    let error : Err?
    let user : User?
}

struct CourierRes : Codable {
    let success : Bool
    let error : Err?
    let courier : Courier?
}

struct User: Codable {
    var email: String?
    var id: String
    var name: Name
    var phone: String?
    var address: Address
    var device: Device?
    var stripeAccount: String?
    var profile: Courier?
    var terms: TermsStatus
    var joinedDate: String

    enum CodingKeys: String, CodingKey {
        case email
        case id = "_id"
        case name
        case phone
        case address
        case stripeAccount = "stripe_account"
        case profile
        case terms
        case joinedDate = "created_at"
    }
}

struct Name: Codable {
    var first: String
    var last: String
}

extension Name {
    func getNameString() -> String{
        return "\(first) \(last)"
    }

    func getNameAndInitial() -> String{
        if (!last.isEmpty) {
            return "\(first) \(last.first!)"
        }
        return "\(first)"
    }
}

struct Address: Codable {
    var street: String
    var city: String
    var state: String
    var zip: String
}

extension Address {
    enum Length {
        case short
        case full
    }
    func getStreet(type: Length) -> String{
        return street
    }
    func getLocaleAddress(type: Length) -> String{
        switch (type) {
        case .full:
            return "\(city), \(state) \(zip)"
        case .short:
            return "\(city), \(zip)"
        }
    }
}

struct Courier: Codable {
    var id: String?
    var name: Name
    var phone: String?
    var location: Location?
    var active: Bool

    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case name
        case phone
        case active
        case location
    }
}

struct Location: Codable {
    var coordinates: Array<Double>?
}

struct Device: Codable {
    var os: String?
    var type: Name?
    var id: String?
    var appVersion: String?
    enum CodingKeys: String, CodingKey {
        case os
        case type
        case id
        case appVersion = "app_version"
    }
}

struct TermsStatus: Codable {
    var version: Int?
    var agreedOn: String?
    enum CodingKeys: String, CodingKey {
        case version
        case agreedOn = "agreed_on"
    }
}



//
//  Formatter.swift
//  Posta
//
//  Created by Dan Rudolf on 7/11/18.
//  Copyright © 2018 com.rudolfmedia. All rights reserved.
//

import UIKit
import CoreLocation

class Formatter: NSObject {

    //MARK: Date formatters
    func getTime(str: String) -> String {
        let df1 = DateFormatter.init()
        df1.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZZZZZ"
        guard let d = df1.date(from: str) else {
            return ""
        }
        let f = DateFormatter.init()
        f.locale = Locale.current
        f.timeZone = TimeZone.current
        f.timeStyle = .short
        return f.string(from: d)
    }

    func getJoinedDate(str: String) -> String {
        let df1 = DateFormatter.init()
        df1.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZZZZZ"
        guard let d = df1.date(from: str) else {
            return ""
        }
        let f = DateFormatter.init()
        f.locale = Locale.current
        f.timeZone = TimeZone.current
        f.dateFormat = "MMMM yyyy"
        return f.string(from: d)
    }

    func getDate(str: String) -> String{
        let df1 = DateFormatter.init()
        df1.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZZZZZ"
        guard let d = df1.date(from: str) else {
            return ""
        }
        let f = DateFormatter.init()
        f.locale = Locale.current
        f.timeZone = TimeZone.current
        f.dateFormat = "MMM d, yyyy"
        return f.string(from: d)
    }

    func getShortDate(str: String) -> String{
        let df1 = DateFormatter.init()
        df1.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZZZZZ"
        guard let d = df1.date(from: str) else {
            return ""
        }
        let f = DateFormatter.init()
        f.locale = Locale.current
        f.timeZone = TimeZone.current
        f.dateStyle = .short
        return f.string(from: d)
    }

    func getDateAndTime(str: String) -> String{
        let df1 = DateFormatter.init()
        df1.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZZZZZ"
        guard let d = df1.date(from: str) else {
            return ""
        }
        let f = DateFormatter.init()
        f.locale = Locale.current
        f.timeZone = TimeZone.current
        f.dateFormat = "MMM d, h:mm a"
        return f.string(from: d)
    }

    func getCurrencyValue(val: Int) -> String{
        let floatVal = Float.init(exactly: val)!
        let dollars = floatVal / 100.00
        let nf = NumberFormatter()
        nf.usesGroupingSeparator = true
        nf.locale = Locale.current
        nf.numberStyle = .currency
        return nf.string(from: NSNumber.init(value: dollars))!
    }

    func getTimeInterval(start: String, end: String)-> Int{
        let df1 = DateFormatter.init()
        df1.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZZZZZ"
        guard let date1 = df1.date(from: end) else {
            return 0
        }
        guard let date2 = df1.date(from: start) else {
            return 0
        }
        let interval = date1.timeIntervalSince(date2)
        let min = (interval/60)
        return Int.init(min.rounded())
    }

    func convertToMiles(meteres: Double) -> Double{
        let mi = meteres * 0.000621371
        let rounded = (mi*10).rounded()/10
        return rounded
    }

    func getDistancbetween(current: CLLocationCoordinate2D, des: CLLocationCoordinate2D) -> String{
        let d = CLLocation.init(latitude: current.latitude, longitude: current.longitude).distance(from: CLLocation.init(latitude: des.latitude, longitude: des.longitude))
        let mi = convertToMiles(meteres: d)
        return "\(mi)"
    }

    func getElapsedTimeDistanceString(trip: Trip) -> String{
        let distance = getDistancbetween(current: (trip.pickupGeo.getCoordinates()), des: (trip.destintationGeo.getCoordinates()))
        guard let received = trip.timestamps.received, let delivered = trip.timestamps.delivered else {
            return "Not Delivered"
        }
        let elapsed = getTimeInterval(start: received, end: delivered)
        return "\(distance)mi \u{2022} \(elapsed)min"
    }

}

//
//  Routes.swift
//  Posta
//
//  Created by Dan Rudolf on 6/15/18.
//  Copyright © 2018 com.rudolfmedia. All rights reserved.
//

import Foundation

struct Methods {
    let GET = "GET"
    let POST = "POST"
    let PUT = "PUT"
}

struct Routes {
    var post = POST()
    var get = GET()
    var put = PUT()
}

struct POST {
    var loginV1 = "v1/auth/courier"
    var newTripV1 = "v1/trips"
    var updatePasswordV1 = "v1/users/"
    var requestDeviceKeyV1 = "/v1/auth/reset"
    var confrimDeviceV1 = "/v1/auth/device"
    var resetPasswordV1 = "/v1/auth/user"
}

struct GET {
    var loginV1 = "v1/auth/courier"
    var regionV1 = "v1/region"
    var tripsV1 = "v1/trips/"
    var stripeLinkV1 = "v1/stripe/account/"
}

struct PUT {
    var updateLocationV1 = "v1/sender"
    var updateStatusV1 = "v1/trips/"
    var updateUserV1 = "v1/users/"
    var updateTermsAgreementV1 = "v1/users/tos/"
}

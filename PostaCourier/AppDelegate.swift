//
//  AppDelegate.swift
//  PostaCourier
//
//  Created by Dan Rudolf on 7/13/18.
//  Copyright © 2018 com.rudolfmedia. All rights reserved.
//

import UIKit
import UserNotifications

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate {

    var window: UIWindow?
    var requestedTrip: Trip?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        UIApplication.shared.applicationIconBadgeNumber = 0
        UNUserNotificationCenter.current().requestAuthorization(options: [.sound, .alert, .badge]) {
            granted, error in
            if granted {
                print("Approval granted to send notifications")
            } else {
                print(error as Any)
            }
        }
        UNUserNotificationCenter.current().delegate = self

        return true
    }

    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {

        guard let rootVC = self.window?.rootViewController else {
            return
        }

        guard let presentedView = rootVC.presentedViewController else {
            return
        }

        guard !(presentedView is TripRequestVC) else{
            return
        }

        completionHandler([.alert, .badge, .sound])

    }

    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        presentTripRequest()
    }

    func presentTripRequest(){
        guard let applicationStateController = self.window?.rootViewController as? ApplicationStateController else{
            return
        }

        guard let trip = requestedTrip else{
            return
        }

        var tripRequestVC: TripRequestVC?
        tripRequestVC = TripRequestVC.init(nibName: "TripRequestVC", bundle: Bundle.main)
        tripRequestVC!.modalPresentationStyle = .overCurrentContext
        tripRequestVC?.currentTrip = trip
        applicationStateController.present(tripRequestVC!, animated: false, completion: nil)

    }


    func triggerNewTripNotification(trip: Trip, distance: String){
        let content = UNMutableNotificationContent()
        content.title = "New Courier Request!"
        content.subtitle = trip.pickup.address.getStreet(type: .full)
        content.body = distance
        content.sound = UNNotificationSound.default()
        content.badge = 1
        content.categoryIdentifier = "message"

        self.requestedTrip = trip

        let trigger = UNTimeIntervalNotificationTrigger.init(timeInterval: 0.01, repeats: false)
        let id = "NEW_TIRP\(arc4random_uniform(20))"
        let request = UNNotificationRequest(identifier: id, content: content, trigger: trigger)
        let center = UNUserNotificationCenter.current()
        center.add(request) {(error) in }
        Timer.scheduledTimer(withTimeInterval: 58.0,
                             repeats: false,
                             block: { (timer) in
                                center.removeAllDeliveredNotifications()
                                UIApplication.shared.applicationIconBadgeNumber = 0
        })

    }

    func triggerCanceledTripDelegate(canceled: Trip){
        let content = UNMutableNotificationContent()
        content.title = "Trip Canceled"
        content.subtitle = canceled.pickup.address.getStreet(type: .full)
        content.body = "This trip has been canceled by the sender. Details are now available in your Delivery History"
        content.sound = UNNotificationSound.default()
        content.badge = 1
        content.categoryIdentifier = "message"

        let trigger = UNTimeIntervalNotificationTrigger.init(timeInterval: 0.01, repeats: false)
        let id = "NEW_TIRP\(arc4random_uniform(20))"
        let request = UNNotificationRequest(identifier: id, content: content, trigger: trigger)
        let center = UNUserNotificationCenter.current()
        center.add(request, withCompletionHandler: nil)
    }


    func invalidateNotification(_ timer: Timer){
        if let id = timer.userInfo as? String{
            UNUserNotificationCenter.current().removeDeliveredNotifications(withIdentifiers: [id])
        }
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

